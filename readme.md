SinsSanity
=====

SinsSanity is a modern map editor for Sins of a Solar Empire, seeking to extend the functionality of GalaxyForge - the map editor made by StarDock. GalaxyForge is the most well-known and widely used map editor, but there is some functionality that is missing. Here's some of the reasons to use SinsSanity:

* Full undo and redo support.
* Copy & Paste entities, such as Planets or entire Star clusters.
* Add/Remove/Edit entites, including Planets, Stars, Hyperlanes & Wormholes.
* Ability to drag Stars and Planets after placing them.
* Scroll wheel support for zooming in and out.
* Applying math functions to existing entities such as: Rotate Entity X degrees Around Star, Duplicate Entity X times Around Star. (See Future Improvements)

![SinsSanity](SinsSanity.png?raw=true)

How to Use SinsSanity
----------------

Clone the repo locally, once downloaded SinsSanity can be built by running `yarn start` in the command line of the root directory of the project.

Alternatively, to create a distribution application build run `yarn package` in the command line of the root directory of the project.
Once the packaging is completed an executable (.exe) is created. Run that file then follow the on-screen instructions to install SinsSanity. Once SinsSanity is installed, run it from the start menu.

When you first run SinsSanity, no map will have been loaded. Open a map by `File -> Open` and select the .galaxy map you wish to open.

The rest is up to you. Create new planets and stars, move entities around and edit features and global map-settings from the sidebar.

Settings
--------

SinsSanity Settings and Preferences have not been created yet but will be added in future versions

Future Improvements
-------------------

The main focus is replicating the existing functionality of GalaxyForge, once that is complete then the ability to perform convienence tasks like the Math functions can be added. 
