import store from "../store";
import { removeEmpties } from "./exportHelpers";
import getMapExport, { getInterStarExport } from "./getMainMapExport";
import getPlayerExport from "./getPlayerExport";
import getStarExport from "./getStarExport";
import getTemplateExport from "./getTemplateExporter";

const Exporter = () => {
    const state = store.getState().mapSettings;

    let result = getMapExport(state);
    result += getStarExport(state.starArray);
    result += getInterStarExport(state.interStarConnectionArray);
    result += getPlayerExport(state.playerArray);
    result += getTemplateExport(state.templateArray);

    result = removeEmpties(result.split("\n")).join("\n");

    return result + '\n';
}

export default Exporter;
