import { PlanetItemsStartToken, PlanetStartToken, PlanetTokens } from "../parser/GalaxyReference";
import { planet } from "../reducers/planetReducer";
import { wsq, posStr, fdp, sbu, addIndentation } from "./exportHelpers";
import { getTemplateFrom } from "./getTemplateExporter";

const getPlanets = (planetArray: planet[]) => {

    const planetStart = (planet: planet) => {
        return ([
            PlanetStartToken + "\n",
            ...addIndentation([
                PlanetTokens[0] + " " + wsq(planet.designName),
                PlanetTokens[1] + " " + wsq(planet.inGameName),
                PlanetTokens[2] + " " + wsq(planet.type),
                PlanetTokens[3] + " " + posStr(planet.pos),
                PlanetTokens[4] + " " + wsq(planet.owner),
                PlanetTokens[5] + " " + sbu(planet.isHomePlanet),
                PlanetTokens[6] + " " + planet.normalStartUpgradeLevelForPopulation,
                PlanetTokens[7] + " " + planet.normalStartUpgradeLevelForCivilianModules,
                PlanetTokens[8] + " " + planet.normalStartUpgradeLevelForTacticalModules,
                PlanetTokens[9] + " " + planet.normalStartUpgradeLevelForArtifacts,
                PlanetTokens[10] + " " + planet.normalStartUpgradeLevelForInfrastructure,
                PlanetTokens[11] + " " + planet.quickStartUpgradeLevelForPopulation,
                PlanetTokens[12] + " " + planet.quickStartUpgradeLevelForCivilianModules,
                PlanetTokens[13] + " " + planet.quickStartUpgradeLevelForTacticalModules,
                PlanetTokens[14] + " " + planet.quickStartUpgradeLevelForArtifacts,
                PlanetTokens[15] + " " + planet.quickStartUpgradeLevelForInfrastructure + "\n"
            ],1,true)
        ]);
    }

    const planetItems = (p: planet) => {
        let pi = p.planetItems;
        return addIndentation([
            PlanetItemsStartToken + "\n",
            ...getTemplateFrom(pi,1)
        ],1,true)
    }

    let result: string[] = []
    for (let i = 0; i < planetArray.length; i++) {
        result[i] = addIndentation([
            ...planetStart(planetArray[i]),
            ...planetItems(planetArray[i]),
            ...addIndentation([
                PlanetTokens[16] + " " + fdp(planetArray[i].spawnProbability),
                PlanetTokens[17] + " " + sbu(planetArray[i].useDefaultTemplate),
                PlanetTokens[18] + " " + planetArray[i].entityCount,
                PlanetTokens[19] + " " + planetArray[i].asteroidCount
            ],1,true)
        ]).join("");
    }

    return result.join("\n");
}

export default getPlanets;
