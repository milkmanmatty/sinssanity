import { pos } from "../reducers/starReducer";

/**
 * Stringifies a boolean to uppercase
 * @param val boolean to stringify.
 * @returns "TRUE" or "FALSE" depending on the respective value of `val`.
 */
export const sbu = (val: boolean) => {
    return val ? "TRUE" : "FALSE";
}

/**
 * Wrap string with double quotes.
 * @param val string to double quotes.
 * @returns `val` wrapped with souble quotes.
 */
export const wsq = (val: string) => {
    return '"' + val + '"';
}

/**
 * Force decimal places. 4 => 4.000000
 * @param value number to force decimal places on.
 * @param decimalPlaces the number of decimal places to add. Default 6.
 * @returns `value` to `decimalPlaces` decimal places.
 */
export const fdp = (value: number, decimalPlaces = 6) => {
    return Number(
        Math.round(
            parseFloat(value + 'e' + decimalPlaces)
        ) + 'e-' + decimalPlaces
    ).toFixed(decimalPlaces);
}

/**
 * Stringifies a `pos` object.
 * @param val `pos` object to stringify
 * @returns `val` represented as: [ x , y ]
 */
export const posStr = (val: pos) => {
    return "[ "+val.x+" , "+val.y+" ]";
}

/**
 * Adds `level` indentations to the start of each element in `src` array.
 * @param src string array to get indented
 * @param level number of times to indent
 * @param suffixNewLine add a newline (`\n`) to the end of each line.
 * @returns `src` array with `level` of indenataion prepended to each element.
 */
export const addIndentation = (src: string[], level = 1, suffixNewLine = false) => {
    let ind = "\t".repeat(level);
    return src.map((str) => {
        return ind + str + (suffixNewLine ? '\n' : '');
    })
}


export const removeEmpties = (src: string[]) => {
    return src.filter((s) => {
        return s.trim().length > 0;
    })
}
