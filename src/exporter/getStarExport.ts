import { StarTokens, StarStartToken } from "../parser/GalaxyReference";
import { star } from "../reducers/starReducer";
import { addIndentation, fdp, posStr, wsq } from "./exportHelpers";
import getConnections from "./getConnections";
import getPlanets from "./getPlanetsExport";

const getStarExport = (stars:star[]) => {
    let result = "";
    for(let i = 0 ; i < stars.length; i++){
        result += [
            StarStartToken,
            ...addIndentation([
                StarTokens[0] + " " + wsq(stars[i].designName),
                StarTokens[1] + " " + wsq(stars[i].inGameName),
                StarTokens[2] + " " + wsq(stars[i].type),
                StarTokens[3] + " " + posStr(stars[i].pos),
                StarTokens[4] + " " + fdp(stars[i].radius),
                StarTokens[5] + " " + stars[i].planetArray.length
            ]),
            getPlanets(stars[i].planetArray),
            ...addIndentation([
                StarTokens[6] + " " + stars[i].connectionArray.length
            ]),
            getConnections(stars[i].connectionArray),
            ...addIndentation([
                StarTokens[7] + " " + stars[i].entityCount,
                StarTokens[8] + " " + fdp(stars[i].spawnProbability) + "\n",
            ]),
        ].join("\n")
    }
    return result;
}

export default getStarExport;
