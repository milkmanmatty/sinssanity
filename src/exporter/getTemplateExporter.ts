import {
    ConditionStartToken,
    ConditionTokens,
    GroupStartToken,
    GroupTokens,
    TemplateStartToken,
    TemplateTokens,
    MapSettingTokens as MapTokens
} from "../parser/GalaxyReference";
import { group } from "../reducers/groupReducer";
import { template } from "../reducers/templateReducer";
import { addIndentation, fdp, wsq } from "./exportHelpers";

const getTemplateExport = (state: template[]) => {
    return ([
        MapTokens[35] + " " + state.length,
        ...state.map((t) => {
            return ([
                TemplateStartToken,
                getTemplateFrom(t,1)
            ].join('\n')
            );
        })
    ].join('\n'))
}

export const getTemplateFrom = (state: template, indentation = 3) => {
    const getTemplate = (t: template) => {
        return addIndentation([
            TemplateTokens[0] + " " + wsq(t.templateName),
            TemplateTokens[1] + " " + t.subTemplates,
            getSubTemplates(t),
            TemplateTokens[3] + " " + t.groupArray.length,
            ...getGroups(t)
        ],indentation,true)
    }

    const getSubTemplates = (t: template) => {
        if (t.subTemplateArray && t.subTemplateArray.length > 1) {
            return t.subTemplateArray.map((sub) => {
                return TemplateTokens[2] + " " + wsq(sub.template)
            }).join("\n")
        }
        return "";
    }

    const getGroups = (t: template) => {
        if (t.groupArray && t.groupArray.length > 1) {
            return t.groupArray.map((grp) => {
                return [
                    GroupStartToken + "\n",
                    ...addIndentation([
                        ConditionStartToken,
                        ...addIndentation([
                            ConditionTokens[0] + " " + wsq(grp.condition.type),
                            ...addIndentation([
                                ConditionTokens[1] + " " + wsq(grp.condition.param)
                            ],3,true)
                        ],1,true).join("\n"),
                        GroupTokens[0] + " " + wsq(grp.owner),
                        GroupTokens[1] + " " + fdp(grp.colonizeChance),
                        GroupTokens[2] + " " + grp.itemsArray.length,
                        getItems(grp) + "\n"
                    ],3,true)
                ].join("\n")
            })
        }
        return [""];
    }

    const getItems = (grp: group) => {
        if (grp.itemsArray && grp.itemsArray.length > 1) {
            return grp.itemsArray.map((item) => {
                return GroupTokens[3] + " " + wsq(item.item)
            })
        }
        return "";
    }

    return getTemplate(state);
}

export default getTemplateExport;
