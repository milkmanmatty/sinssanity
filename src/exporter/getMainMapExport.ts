import {
    InterStarConnectionStartToken,
    InterStarConnectionTokens,
    MapSettingTokens as MapTokens,
    MapStartToken
} from "../parser/GalaxyReference";
import { interStarConnection } from "../reducers/interStarConnectionReducer";
import { MapSettings } from "../reducers/mapSettingsReducer";
import { addIndentation, fdp, sbu, wsq } from "./exportHelpers";

const getMapExport = (state: MapSettings) => {
    const gameTypes = () => {
        const strArr = state.recommendedGameTypes.map((rgt) => {
            return "recommendedGameType " + wsq(rgt.recommendedGameType);
        });
        return strArr.join("\n");
    }

    return ([
        MapStartToken,
        (
            state.sinsArchiveVersion === -1 ?
            "" :
            MapTokens[0] + " " + state.sinsArchiveVersion
        ),
        MapTokens[1] + " " + state.versionNumber,
        MapTokens[2] + " " + sbu(state.isBrowsable),
        MapTokens[3] + " " + wsq(state.browsePictureName),
        MapTokens[4] + " " + wsq(state.browseName),
        MapTokens[5] + " " + wsq(state.browseDescription),
        MapTokens[6] + " " + sbu(state.isFirstCapitalShipIsFlagship),
        MapTokens[7] + " " + sbu(state.randomizeStartingPositions),
        MapTokens[8] + " " + fdp(state.planetArtifactDensity),
        MapTokens[9] + " " + fdp(state.planetBonusDensity),
        MapTokens[10] + " " + state.normalStartHomePlanetUpgradeLevel_Population,
        MapTokens[11] + " " + state.normalStartHomePlanetUpgradeLevel_CivilianModules,
        MapTokens[12] + " " + state.normalStartHomePlanetUpgradeLevel_TacticalModules,
        MapTokens[13] + " " + state.normalStartHomePlanetUpgradeLevel_Home,
        MapTokens[14] + " " + state.normalStartHomePlanetUpgradeLevel_ArtifactLevel,
        MapTokens[15] + " " + state.normalStartHomePlanetUpgradeLevel_Infrastructure,
        MapTokens[16] + " " + state.quickStartHomePlanetUpgradeLevel_Population,
        MapTokens[17] + " " + state.quickStartHomePlanetUpgradeLevel_CivilianModules,
        MapTokens[18] + " " + state.quickStartHomePlanetUpgradeLevel_TacticalModules,
        MapTokens[19] + " " + state.quickStartHomePlanetUpgradeLevel_Home,
        MapTokens[20] + " " + state.quickStartHomePlanetUpgradeLevel_ArtifactLevel,
        MapTokens[21] + " " + state.quickStartHomePlanetUpgradeLevel_Infrastructure,
        MapTokens[22] + " " + state.recommendedGameTypes.length,
        gameTypes(),
        MapTokens[24] + " " + fdp(state.metersPerGalaxyUnit),
        MapTokens[25] + " " + fdp(state.pixelsPerGalaxyUnit),
        MapTokens[26] + " " + sbu(state.useRandomGenerator),
        MapTokens[27] + " " + fdp(state.galaxyWidth),
        MapTokens[28] + " " + fdp(state.galaxyHeight),
        MapTokens[29] + " " + state.starArray.length,
        MapTokens[30] + " " + state.planetCount,
        MapTokens[31] + " 0",
        MapTokens[32] + " " + state.starArray.length + "\n",
    ].join("\n")
    );
}

export const getInterStarExport = (interStarsCons: interStarConnection[]) => {
    const getInterStars = (interStars: interStarConnection[]) => {
        return interStars.map((isc) => {
            return ([
                InterStarConnectionStartToken,
                ...addIndentation([
                    InterStarConnectionTokens[0] + " " + isc.starIndexA,
                    InterStarConnectionTokens[1] + " " + isc.planetIndexA,
                    InterStarConnectionTokens[2] + " " + isc.starIndexB,
                    InterStarConnectionTokens[3] + " " + isc.planetIndexB,
                    InterStarConnectionTokens[4] + " " + fdp(isc.spawnProbability),
                    InterStarConnectionTokens[5] + " " + wsq(isc.type),
                ],1,true)
            ].join('\n')
            );
        }).join('\n');
    }

    return [
        MapTokens[33] + " " + interStarsCons.length,
        getInterStars(interStarsCons)
    ].join('\n');
}

export default getMapExport;
