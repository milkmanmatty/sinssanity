import {
    PlayerStartToken,
    PlayerTokens,
    MapSettingTokens as MapTokens
} from "../parser/GalaxyReference";
import { player } from "../reducers/playerReducer";
import { addIndentation, sbu, wsq } from "./exportHelpers";

const getPlayerExport = (state: player[]) => {
    let result = state.map((p) => {
        return ([
            PlayerStartToken,
            ...addIndentation([
                PlayerTokens[0] + " " + wsq(p.designName),
                PlayerTokens[1] + " " + wsq(p.inGameName),
                PlayerTokens[2] + " " + wsq(p.overrideRaceName),
                PlayerTokens[3] + " " + p.startingCredits,
                PlayerTokens[4] + " " + p.startingMetal,
                PlayerTokens[5] + " " + p.startingCrystal,
                PlayerTokens[6] + " " + p.startingCrystal,
                PlayerTokens[7] + " " + sbu(p.isNormalPlayer),
                PlayerTokens[8] + " " + sbu(p.isRaidingPlayer),
                PlayerTokens[9] + " " + sbu(p.isInsurgentPlayer),
                PlayerTokens[10] + " " + sbu(p.isOccupationPlayer),
                PlayerTokens[11] + " " + sbu(p.isMadVasariPlayer),
                PlayerTokens[12] + " " + wsq(p.themeGroup),
                PlayerTokens[13] + " " + p.themeIndex,
                PlayerTokens[14] + " " + wsq(p.pictureGroup),
                PlayerTokens[15] + " " + p.pictureIndex,
            ],1,true)
        ].join('\n'));
    });

    return [
        MapTokens[34] + " " + state.length,
        ...result
    ].join("\n");
}

export default getPlayerExport;
