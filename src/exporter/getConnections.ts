import { ConnectionStartToken, ConnectionTokens } from "../parser/GalaxyReference";
import { connection } from "../reducers/connectionReducer";
import { addIndentation, fdp, wsq } from "./exportHelpers";

const getConnections = (conArray: connection[]) => {
    return conArray.map((con) => {
        return [
            '\t' + ConnectionStartToken,
            ...addIndentation([
                ConnectionTokens[0] + " " + con.planetIndexA,
                ConnectionTokens[1] + " " + con.planetIndexB,
                ConnectionTokens[2] + " " + fdp(con.spawnProbability),
                ConnectionTokens[3] + " " + wsq(con.type) + "\n"
            ],2)
        ].join("\n")
    }).join("\n")
}

export default getConnections;
