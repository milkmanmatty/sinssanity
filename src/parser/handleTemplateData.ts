import * as Template from "../reducers/templateReducer";
import store from "../store";
import { rmq, splitOnce } from "./helperFunctions";

const handleTemplateData = (line: string) => {
    const dispatch = store.dispatch;
    let [key, value] = splitOnce(line);
    switch (key) {
        case "templateName":
            dispatch(Template.setTemplateName(
                rmq(value)
            ));
            break;
        case "subTemplates":
            dispatch(Template.setSubTemplates(
                rmq(value)
            ));
            break;
        default:
            console.log("Template line ignored. Could not categorize line: " + line)
            break;
    }
}

export default handleTemplateData;
