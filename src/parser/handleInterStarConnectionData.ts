import * as InterStarConnection from "../reducers/interStarConnectionReducer";
import store from "../store";
import { rmq, splitOnce } from "./helperFunctions";

const handleInterStarConnectionData = (line: string) => {
    const dispatch = store.dispatch;
    let [key, value] = splitOnce(line);
    switch (key) {
        case "starIndexA":
            dispatch(InterStarConnection.setStarIndexA(
                parseInt(value, 10)
            ));
            break;
        case "planetIndexA":
            dispatch(InterStarConnection.setPlanetIndexA(
                parseInt(value, 10)
            ));
            break;
        case "starIndexB":
            dispatch(InterStarConnection.setStarIndexB(
                parseInt(value, 10)
            ));
            break;
        case "planetIndexB":
            dispatch(InterStarConnection.setPlanetIndexB(
                parseInt(value, 10)
            ));
            break;
        case "spawnProbability":
            dispatch(InterStarConnection.setSpawnProbability(
                parseFloat(value)
            ));
            break;
        case "type":
            dispatch(InterStarConnection.setType(
                rmq(value)
            ));
            break;
        default:
            console.log("InterStarConnection line ignored. Could not categorize line: " + line)
            break;
    }
}

export default handleInterStarConnectionData;
