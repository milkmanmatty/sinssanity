import * as Player from "../reducers/playerReducer";
import store from "../store";
import { rmq, splitOnce } from "./helperFunctions";

const handlePlayerData = (line: string) => {
    const dispatch = store.dispatch;
    let [key, value] = splitOnce(line);
    switch (key) {
        case "designName":
            dispatch(Player.setDesignName(
                rmq(value)
            ));
            break;
        case "inGameName":
            dispatch(Player.setInGameName(
                rmq(value)
            ));
            break;
        case "overrideRaceName":
            dispatch(Player.setOverrideRaceName(
                rmq(value)
            ));
            break;
        case "teamIndex":
            dispatch(Player.setOverrideRaceName(
                rmq(value)
            ));
            break;
        case "startingCredits":
            break;
        case "startingMetal":
            break;
        case "startingCrystal":
            break;
        case "isNormalPlayer":
            break;
        case "isRaidingPlayer":
            break;
        case "isInsurgentPlayer":
            break;
        case "isOccupationPlayer":
            break;
        case "isMadVasariPlayer":
            break;
        case "themeGroup":
            break;
        case "themeIndex":
            break;
        case "pictureGroup":
            break;
        case "pictureIndex":
            break;
        default:
            console.log("Player line ignored. Could not categorize line: " + line)
            break;
    }
}

export default handlePlayerData;
