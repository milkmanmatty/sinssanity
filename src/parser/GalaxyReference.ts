export const MapStartToken = "TXT";
export const MapSettingTokens = [
    "SinsArchiveVersion",
    "versionNumber",
    "isBrowsable",
    "browsePictureName",
    "browseName",
    "browseDescription",
    "isFirstCapitalShipIsFlagship",
    "randomizeStartingPositions",
    "planetArtifactDensity",
    "planetBonusDensity",
    "normalStartHomePlanetUpgradeLevel:Population",
    "normalStartHomePlanetUpgradeLevel:CivilianModules",
    "normalStartHomePlanetUpgradeLevel:TacticalModules",
    "normalStartHomePlanetUpgradeLevel:Home",
    "normalStartHomePlanetUpgradeLevel:ArtifactLevel",
    "normalStartHomePlanetUpgradeLevel:Infrastructure",
    "quickStartHomePlanetUpgradeLevel:Population",
    "quickStartHomePlanetUpgradeLevel:CivilianModules",
    "quickStartHomePlanetUpgradeLevel:TacticalModules",
    "quickStartHomePlanetUpgradeLevel:Home",
    "quickStartHomePlanetUpgradeLevel:ArtifactLevel",
    "quickStartHomePlanetUpgradeLevel:Infrastructure",
    "recommendedGameTypeCount",
    "recommendedGameType",
    "metersPerGalaxyUnit",
    "pixelsPerGalaxyUnit",
    "useRandomGenerator",
    "galaxyWidth",
    "galaxyHeight",
    "nextStarNameUniqueId",
    "nextPlanetNameUniqueId",
    "triggerCount",
    "starCount",
    "interStarConnectionCount",
    "playerCount",
    "templates"
];

export const StarStartToken = "star";
export const StarTokens = [
    "designName",
    "inGameName",
    "type",
    "pos",
    "radius",
    "planetCount",
    "connectionCount",
    "entityCount",
    "spawnProbability"
];

export const PlanetStartToken = "planet";
export const PlanetTokens = [
    "designName",
    "inGameName",
    "type",
    "pos",
    "owner",
    "isHomePlanet",
    "normalStartUpgradeLevelForPopulation",
    "normalStartUpgradeLevelForCivilianModules",
    "normalStartUpgradeLevelForTacticalModules",
    "normalStartUpgradeLevelForArtifacts",
    "normalStartUpgradeLevelForInfrastructure",
    "quickStartUpgradeLevelForPopulation",
    "quickStartUpgradeLevelForCivilianModules",
    "quickStartUpgradeLevelForTacticalModules",
    "quickStartUpgradeLevelForArtifacts",
    "quickStartUpgradeLevelForInfrastructure",
    "spawnProbability",
    "useDefaultTemplate",
    "entityCount",
    "asteroidCount"
];

export const PlanetItemsStartToken = "planetItems";
export const PlanetItemsTokens = [
    "templateName",
    "subTemplates",
    "template",
    "groups",
    "group"
];

export const GroupStartToken = "group";
export const GroupTokens = [
    "owner",
    "colonizeChance",
    "items",
    "item"
];
export const ConditionStartToken = "condition";
export const ConditionTokens = [
    "type",
    "param"
];

export const ConnectionStartToken = "connection";
export const ConnectionTokens = [
    "planetIndexA",
    "planetIndexB",
    "spawnProbability",
    "type"
];

export const InterStarConnectionStartToken = "interStarConnection";
export const InterStarConnectionTokens = [
    "starIndexA",
    "planetIndexA",
    "starIndexB",
    "planetIndexB",
    "spawnProbability",
    "type"
];

export const PlayerStartToken = "player";
export const PlayerTokens = [
    "designName",
    "inGameName",
    "overrideRaceName",
    "teamIndex",
    "startingCredits",
    "startingMetal",
    "startingCrystal",
    "isNormalPlayer",
    "isRaidingPlayer",
    "isInsurgentPlayer",
    "isOccupationPlayer",
    "isMadVasariPlayer",
    "themeGroup",
    "themeIndex",
    "pictureGroup",
    "pictureIndex"
];

export const TemplateStartToken = "template";
export const TemplateTokens = [
    "templateName",
    "subTemplates",
    "template",
    "groups",
    "group"
];

export const DuplicateTokens = [
    "designName",
    "inGameName",
    "type",
    "pos",
    "spawnProbability",
    "templateName",
    "subTemplates"
];
