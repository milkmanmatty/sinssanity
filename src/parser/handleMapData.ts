import store from "../store";
import * as Map from "../reducers/mapSettingsReducer";
import { rmq, splitOnce } from "./helperFunctions";

const handleMapData = (line: string) => {
    const dispatch = store.dispatch;
    let [key, value] = splitOnce(line);
    switch (key) {
        case "SinsArchiveVersion":
            dispatch(Map.setSinsArchiveVersion(
                parseInt(value, 10)
            ));
            break;
        case "versionNumber":
            dispatch(Map.setVersionNumber(
                parseFloat(value)
            ));
            break;
        case "isBrowsable":
            dispatch(Map.setIsBrowsable(
                value === "TRUE" ? true : false
            ));
            break;
        case "browsePictureName":
            dispatch(Map.setBrowsePictureName(
                rmq(value)
            ));
            break;
        case "browseName":
            dispatch(Map.setBrowseName(
                rmq(value)
            ));
            break;
        case "browseDescription":
            dispatch(Map.setBrowseDescription(
                rmq(value)
            ));
            break;
        case "isFirstCapitalShipIsFlagship":
            dispatch(Map.setIsFirstCapitalShipIsFlagship(
                value === "TRUE" ? true : false
            ));
            break;
        case "randomizeStartingPositions":
            dispatch(Map.setRandomizeStartingPositions(
                value === "TRUE" ? true : false
            ));
            break;
        case "planetArtifactDensity":
            dispatch(Map.setPlanetArtifactDensity(
                parseFloat(value)
            ));
            break;
        case "planetBonusDensity":
            dispatch(Map.setPlanetBonusDensity(
                parseFloat(value)
            ));
            break;
        case "normalStartHomePlanetUpgradeLevel:Population":
            dispatch(Map.setNormal_Population(
                parseInt(value, 10)
            ));
            break;
        case "normalStartHomePlanetUpgradeLevel:CivilianModules":
            dispatch(Map.setNormal_CivilianModules(
                parseInt(value, 10)
            ));
            break;
        case "normalStartHomePlanetUpgradeLevel:TacticalModules":
            dispatch(Map.setNormal_TacticalModules(
                parseInt(value, 10)
            ));
            break;
        case "normalStartHomePlanetUpgradeLevel:Home":
            dispatch(Map.setNormal_Home(
                parseInt(value, 10)
            ));
            break;
        case "normalStartHomePlanetUpgradeLevel:ArtifactLevel":
            dispatch(Map.setNormal_ArtifactLevel(
                parseInt(value, 10)
            ));
            break;
        case "normalStartHomePlanetUpgradeLevel:Infrastructure":
            dispatch(Map.setNormal_Infrastructure(
                parseInt(value, 10)
            ));
            break;
        case "quickStartHomePlanetUpgradeLevel:Population":
            dispatch(Map.setQuick_Population(
                parseInt(value, 10)
            ));
            break;
        case "quickStartHomePlanetUpgradeLevel:CivilianModules":
            dispatch(Map.setQuick_CivilianModules(
                parseInt(value, 10)
            ));
            break;
        case "quickStartHomePlanetUpgradeLevel:TacticalModules":
            dispatch(Map.setQuick_TacticalModules(
                parseInt(value, 10)
            ));
            break;
        case "quickStartHomePlanetUpgradeLevel:Home":
            dispatch(Map.setQuick_Home(
                parseInt(value, 10)
            ));
            break;
        case "quickStartHomePlanetUpgradeLevel:ArtifactLevel":
            dispatch(Map.setQuick_ArtifactLevel(
                parseInt(value, 10)
            ));
            break;
        case "quickStartHomePlanetUpgradeLevel:Infrastructure":
            dispatch(Map.setQuick_Infrastructure(
                parseInt(value, 10)
            ));
            break;
        case "recommendedGameTypeCount":
            break;
        case "recommendedGameType":
            dispatch(Map.addRecommendedGameType(
                { recommendedGameType: rmq(value) }
            ));
            break;
        case "metersPerGalaxyUnit":
            dispatch(Map.setMetersPerGalaxyUnit(
                parseFloat(value)
            ));
            break;
        case "pixelsPerGalaxyUnit":
            dispatch(Map.setPixelsPerGalaxyUnit(
                parseFloat(value)
            ));
            break;
        case "useRandomGenerator":
            dispatch(Map.setUseRandomGenerator(
                value === "TRUE" ? true : false
            ));
            break;
        case "galaxyWidth":
            dispatch(Map.setGalaxyWidth(
                parseFloat(value)
            ));
            break;
        case "galaxyHeight":
            dispatch(Map.setGalaxyHeight(
                parseFloat(value)
            ));
            break;
        case "nextStarNameUniqueId":
            break;
        case "nextPlanetNameUniqueId":
            break;
        case "triggerCount":
            break;
        case "starCount":
            break;
        case "interStarConnectionCount":
            break;
        case "playerCount":
            break;
        case "templates":
            break;
        default:
            console.log("Map line ignored. Could not categorize line: " + line)
            break;
    }
}

export default handleMapData;
