import * as Condition from "../reducers/conditionReducer";
import store from "../store";
import { rmq, splitOnce } from "./helperFunctions";

const handleConditionData = (line: string) => {
    const dispatch = store.dispatch;
    let [key, value] = splitOnce(line);
    switch (key) {
        case "type":
            dispatch(Condition.setType(
                rmq(value)
            ));
            break;
        case "param":
            dispatch(Condition.setParam(
                rmq(value)
            ));
            break;
        default:
            console.log("Condition line ignored. Could not categorize line: " + line)
            break;
    }
}

export default handleConditionData;
