import * as Planet from "../reducers/planetReducer";
import store from "../store";
import { parsePos, rmq, splitOnce } from "./helperFunctions";

const handlePlanetData = (line: string) => {
    const dispatch = store.dispatch;
    let [key, value] = splitOnce(line);
    switch (key) {
        case "designName":
            dispatch(Planet.setDesignName(
                rmq(value)
            ));
            break;
        case "inGameName":
            dispatch(Planet.setInGameName(
                rmq(value)
            ));
            break;
        case "type":
            dispatch(Planet.setType(
                rmq(value)
            ));
            break;
        case "pos":
            dispatch(Planet.setPos(
                parsePos(value)
            ));
            break;
        case "owner":
            dispatch(Planet.setOwner(
                rmq(value)
            ));
            break;
        case "isHomePlanet":
            dispatch(Planet.setIsHomePlanet(
                value === "TRUE" ? true : false
            ));
            break;
        case "normalStartUpgradeLevelForPopulation":
            dispatch(Planet.setNormal_Population(
                parseInt(value, 10)
            ));
            break;
        case "normalStartUpgradeLevelForCivilianModules":
            dispatch(Planet.setNormal_CivilianModules(
                parseInt(value, 10)
            ));
            break;
        case "normalStartUpgradeLevelForTacticalModules":
            dispatch(Planet.setNormal_TacticalModules(
                parseInt(value, 10)
            ));
            break;
        case "normalStartUpgradeLevelForArtifacts":
            dispatch(Planet.setNormal_Artifacts(
                parseInt(value, 10)
            ));
            break;
        case "normalStartUpgradeLevelForInfrastructure":
            dispatch(Planet.setNormal_Infrastructure(
                parseInt(value, 10)
            ));
            break;
        case "quickStartUpgradeLevelForPopulation":
            dispatch(Planet.setQuick_Population(
                parseInt(value, 10)
            ));
            break;
        case "quickStartUpgradeLevelForCivilianModules":
            dispatch(Planet.setQuick_CivilianModules(
                parseInt(value, 10)
            ));
            break;
        case "quickStartUpgradeLevelForTacticalModules":
            dispatch(Planet.setQuick_TacticalModules(
                parseInt(value, 10)
            ));
            break;
        case "quickStartUpgradeLevelForArtifacts":
            dispatch(Planet.setQuick_Artifacts(
                parseInt(value, 10)
            ));
            break;
        case "quickStartUpgradeLevelForInfrastructure":
            dispatch(Planet.setQuick_Infrastructure(
                parseInt(value, 10)
            ));
            break;
        case "spawnProbability":
            dispatch(Planet.setSpawnProbability(
                parseFloat(value)
            ));
            break;
        case "useDefaultTemplate":
            dispatch(Planet.setUseDefaultTemplate(
                value === "TRUE" ? true : false
            ));
            break;
        case "entityCount":
            dispatch(Planet.setEntityCount(
                parseInt(value, 10)
            ));
            break;
        case "asteroidCount":
            dispatch(Planet.setAsteroidCount(
                parseInt(value, 10)
            ));
            break;
        default:
            console.log("Planet line ignored. Could not categorize line: " + line)
            break;
    }
}

export default handlePlanetData;
