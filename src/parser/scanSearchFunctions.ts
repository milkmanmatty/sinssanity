import { commitConditionToGroup, commitConnectionToStar, commitGroupToPlanetItem, commitGroupToTemplate, commitPlanetItemToPlanet, commitPlanetToStar, commitPlayerToMap, commitStarToMap, commitTemplateToMap } from "./commitEntities";
import { ParseSection } from "./GalaxyParser";
import { GroupTokens, PlanetItemsTokens, PlanetTokens, ConnectionTokens, StarTokens, MapSettingTokens } from "./GalaxyReference";
import handleConnectionData from "./handleConnectionData";
import handleGroupData from "./handleGroupData";
import handleMapData from "./handleMapData";
import handlePlanetData from "./handlePlanetData";
import handlePlanetItemData from "./handlePlanetItemsData";
import handlePlayerData from "./handlePlayerData";
import handleStarData from "./handleStarData";
import handleTemplateData from "./handleTemplateData";
import { splitOnce } from "./helperFunctions";

export const searchForGroup = (
    line: string,
    curStep: ParseSection
): ParseSection => {
    let token = splitOnce(line)[0];
    if (GroupTokens.includes(token)) {
        switch (curStep) {
            case "TemplateCondition":
            case "Condition": // <--happen often
                commitConditionToGroup();
                break;
            case "Group": // <--should never happen
            case "Map": //Handle the data and hope for the best
            case "Star":
            case "Planet":
            case "Connection":
            case "Player":
            case "Template":
            case "TemplateGroup":
            default:
                break;
        }
        handleGroupData(line);
        return "Group";
    }
    return searchForPlanetItems(line, curStep);
}

export const searchForPlanetItems = (
    line: string,
    curStep: ParseSection
): ParseSection => {
    let token = splitOnce(line)[0];
    if (PlanetItemsTokens.includes(token)) {
        switch (curStep) {
            case "Condition":
                commitConditionToGroup();
                commitGroupToPlanetItem();
                break;
            case "Group":
                commitGroupToPlanetItem();
                break;
            case "PlanetItem": // <--should never happen
            case "Map":
            case "Star":
            case "Planet":
            case "Connection":
            case "Player":
            case "Template":
            case "TemplateGroup":
            case "TemplateCondition":
            default:
                break;
        }
        handlePlanetItemData(line);
        return "PlanetItem";
    }
    return searchForPlanet(line, curStep);
}

export const searchForPlanet = (
    line: string,
    curStep: ParseSection
): ParseSection => {
    let token = splitOnce(line)[0];
    if (PlanetTokens.includes(token)) {
        switch (curStep) {
            case "Condition":
                commitConditionToGroup();
                commitGroupToPlanetItem();
                commitPlanetItemToPlanet();
                break;
            case "Group":
                commitGroupToPlanetItem();
                commitPlanetItemToPlanet();
                break;
            case "PlanetItem":
                commitPlanetItemToPlanet();
                break;
            case "Planet": // <--should never happen
            case "Map":
            case "Star":
            case "Connection":
            case "Player":
            case "Template":
            case "TemplateGroup":
            case "TemplateCondition":
            default:
                break;
        }
        handlePlanetData(line);
        return "Planet";
    }
    return searchForStar(line, curStep);
}

export const searchForConnection = (
    line: string,
    curStep: ParseSection
): ParseSection => {
    let token = splitOnce(line)[0];
    if (ConnectionTokens.includes(token)) {
        switch (curStep) {
            case "Condition":
                commitConditionToGroup();
                commitGroupToPlanetItem();
                commitPlanetItemToPlanet();
                commitPlanetToStar();
                break;
            case "Group":
                commitGroupToPlanetItem();
                commitPlanetItemToPlanet();
                commitPlanetToStar();
                break;
            case "PlanetItem":
                commitPlanetItemToPlanet();
                commitPlanetToStar();
                break;
            case "Planet":
                commitPlanetToStar();
                break;
            case "Map":
            case "Star":
            case "Connection": // <--should never happen
            case "Player":
            case "Template":
            case "TemplateGroup":
            case "TemplateCondition":
            default:
                break;
        }
        handleConnectionData(line);
        return "Connection";
    }
    return searchForStar(line, curStep);
}

export const searchForStar = (
    line: string,
    curStep: ParseSection
): ParseSection => {
    let token = splitOnce(line)[0];
    if (StarTokens.includes(token)) {
        switch (curStep) {
            case "Condition":
                commitConditionToGroup();
                commitGroupToPlanetItem();
                commitPlanetItemToPlanet();
                commitPlanetToStar();
                break;
            case "Group":
                commitGroupToPlanetItem();
                commitPlanetItemToPlanet();
                commitPlanetToStar();
                break;
            case "PlanetItem":
                commitPlanetItemToPlanet();
                commitPlanetToStar();
                break;
            case "Planet":
                commitPlanetToStar();
                break;
            case "Connection":
                commitConnectionToStar();
                break;
            case "Map":
            case "Star": // <--should never happen
            case "Player":
            case "Template":
            case "TemplateGroup":
            case "TemplateCondition":
            default:
                break;
        }
        handleStarData(line);
        return "Star";
    }
    return searchForPlayer(line, curStep);
}

export const searchForPlayer = (
    line: string,
    curStep: ParseSection
): ParseSection => {
    let token = splitOnce(line)[0];
    if (MapSettingTokens.includes(token)) {
        switch (curStep) {
            case "Player": // <--should never happen
            case "Map":
            case "Star":
            case "Planet":
            case "PlanetItem":
            case "Group":
            case "Condition":
            case "Connection":
            case "Template":
            case "TemplateGroup":
            case "TemplateCondition":
            default:
                break;
        }
        handlePlayerData(line);
        return "Player";
    }
    return searchForTemplate(line, curStep);
}

export const searchForTemplate = (
    line: string,
    curStep: ParseSection
): ParseSection => {
    let token = splitOnce(line)[0];
    if (MapSettingTokens.includes(token)) {
        switch (curStep) {
            case "TemplateCondition":
                commitConditionToGroup();
                commitGroupToTemplate();
                commitTemplateToMap();
                break;
            case "TemplateGroup":
                commitGroupToTemplate();
                commitTemplateToMap();
                break;
            case "Template": // <--should never happen
            case "Map":
            case "Star":
            case "Planet":
            case "PlanetItem":
            case "Group":
            case "Condition":
            case "Connection":
            case "Player":
            default:
                break;
        }
        handleTemplateData(line);
        return "Template";
    }
    return searchForMap(line, curStep);
}

export const searchForMap = (
    line: string,
    curStep: ParseSection
): ParseSection => {
    let token = splitOnce(line)[0];
    if (MapSettingTokens.includes(token)) {
        switch (curStep) {
            case "Condition":
                commitConditionToGroup();
                commitGroupToPlanetItem();
                commitPlanetItemToPlanet();
                commitPlanetToStar();
                break;
            case "Group":
                commitGroupToPlanetItem();
                commitPlanetItemToPlanet();
                commitPlanetToStar();
                break;
            case "PlanetItem":
                commitPlanetItemToPlanet();
                commitPlanetToStar();
                break;
            case "Planet":
                commitPlanetToStar();
                break;
            case "Connection":
                commitConnectionToStar();
                break;
            case "Star":
                commitStarToMap();
                break;
            case "Player":
                commitPlayerToMap();
                break;
            case "TemplateCondition":
                commitConditionToGroup();
                commitGroupToTemplate();
                commitTemplateToMap();
                break;
            case "TemplateGroup":
                commitGroupToTemplate();
                commitTemplateToMap();
                break;
            case "Template":
                commitTemplateToMap();
                break;
            case "Map": // <--should never happen
            default:
                break;
        }
        handleMapData(line);
        return "Map";
    }
    return "NotFound";
}
