import * as PlanetItems from "../reducers/planetItemsReducer";
import store from "../store";
import { rmq, splitOnce } from "./helperFunctions";

const handlePlanetItemData = (line: string) => {
    const dispatch = store.dispatch;
    let [key, value] = splitOnce(line);
    switch (key) {
        case "templateName":
            dispatch(PlanetItems.setTemplateName(
                rmq(value)
            ));
            break;
        case "subTemplates":
            dispatch(PlanetItems.setSubTemplates(
                parseInt(value, 10)
            ));
            break;
        case "template":
            dispatch(PlanetItems.addTemplate(
                { template: rmq(value) }
            ));
            break;
        case "groups":
            dispatch(PlanetItems.setGroups(
                parseInt(value, 10)
            ));
            break;
        default:
            console.log("PlanetItem line ignored. Could not categorize line: " + line)
            break;
    }
}

export default handlePlanetItemData;
