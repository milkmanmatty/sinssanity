import * as Group from "../reducers/groupReducer";
import store from "../store";
import { rmq, splitOnce } from "./helperFunctions";

const handleGroupData = (line: string) => {
    const dispatch = store.dispatch;
    let [key, value] = splitOnce(line);
    switch (key) {
        case "owner":
            dispatch(Group.setOwner(
                rmq(value)
            ));
            break;
        case "colonizeChance":
            dispatch(Group.setColonizeChance(
                parseFloat(value)
            ));
            break;
        case "items":
            dispatch(Group.setItems(
                parseInt(value)
            ));
            break;
        case "item":
            dispatch(Group.addItem(
                { item: rmq(value) }
            ));
            break;
        default:
            console.log("Group line ignored. Could not categorize line: " + line)
            break;
    }
}

export default handleGroupData;
