import * as Star from "../reducers/starReducer";
import store from "../store";
import { parsePos, rmq, splitOnce } from "./helperFunctions";

const handleStarData = (line: string) => {
    const dispatch = store.dispatch;
    let [key, value] = splitOnce(line);
    switch (key) {
        case "designName":
            dispatch(Star.setDesignName(
                rmq(value)
            ));
            break;
        case "inGameName":
            dispatch(Star.setInGameName(
                rmq(value)
            ));
            break;
        case "type":
            dispatch(Star.setType(
                rmq(value)
            ));
            break;
        case "pos":
            dispatch(Star.setPos(
                parsePos(value)
            ));
            break;
        case "radius":
            dispatch(Star.setRadius(
                parseFloat(value)
            ));
            break;
        case "entityCount":
            dispatch(Star.setEntityCount(
                parseFloat(value)
            ));
            break;
        case "spawnProbability":
            dispatch(Star.setSpawnProbability(
                parseFloat(value)
            ));
            break;
        case "planetCount":
        case "connectionCount":
            break;
        default:
            console.log("Star line ignored. Could not categorize line: " + line)
            break;
    }
}

export default handleStarData;
