import * as Connection from "../reducers/connectionReducer";
import store from "../store";
import { rmq, splitOnce } from "./helperFunctions";

const handleConnectionData = (line: string) => {
    const dispatch = store.dispatch;
    let [key, value] = splitOnce(line);
    switch (key) {
        case "planetIndexA":
            dispatch(Connection.setPlanetIndexA(
                parseInt(value, 10)
            ));
            break;
        case "planetIndexB":
            dispatch(Connection.setPlanetIndexB(
                parseInt(value, 10)
            ));
            break;
        case "spawnProbability":
            dispatch(Connection.setSpawnProbability(
                parseFloat(value)
            ));
            break;
        case "type":
            dispatch(Connection.setType(
                rmq(value)
            ));
            break;
        default:
            console.log("Connection line ignored. Could not categorize line: " + line)
            break;
    }
}

export default handleConnectionData;
