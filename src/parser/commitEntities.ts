import store from "../store";
import * as MapSettings from "../reducers/mapSettingsReducer";
import * as Star from "../reducers/starReducer";
import * as Planet from "../reducers/planetReducer";
import * as PlanetItems from "../reducers/planetItemsReducer";
import * as Connection from "../reducers/connectionReducer";
import * as InterStarConnection from "../reducers/interStarConnectionReducer";
import * as Player from "../reducers/playerReducer";
import * as Group from "../reducers/groupReducer";
import * as Condition from "../reducers/conditionReducer";
import * as Template from "../reducers/templateReducer";
import { getCurrentCondition, getCurrentConnection, getCurrentGroup, getCurrentISC, getCurrentPlanet, getCurrentPlanetItem, getCurrentPlayer, getCurrentStar, getCurrentTemplate } from "./helperFunctions";

export const commitStarToMap = () => {
    const dispatch = store.dispatch;
    console.log("commit star to map");
    dispatch(MapSettings.addStar(
        getCurrentStar()
    ));
    dispatch(Star.reset());
    dispatch(Star.setIndex(store.getState().mapSettings.starArray.length));
}

export const commitPlanetToStar = () => {
    const dispatch = store.dispatch;
    console.log("commit planet to star");
    dispatch(Star.addPlanet(
        getCurrentPlanet()
    ));
    dispatch(Planet.reset());
    dispatch(MapSettings.incrementPlanetCount());
    dispatch(Planet.setIndex(store.getState().mapSettings.planetCount));
}

export const commitPlanetItemToPlanet = () => {
    const dispatch = store.dispatch;
    dispatch(Planet.setPlanetItems(
        getCurrentPlanetItem()
    ));
    dispatch(PlanetItems.reset())
}

export const commitGroupToPlanetItem = () => {
    const dispatch = store.dispatch;
    dispatch(PlanetItems.addGroup(
        getCurrentGroup()
    ));
    dispatch(Group.reset());
}

export const commitConditionToGroup = () => {
    const dispatch = store.dispatch;
    dispatch(Group.setCondition(
        getCurrentCondition()
    ));
    dispatch(Condition.reset());
}

export const commitConnectionToStar = () => {
    const dispatch = store.dispatch;
    dispatch(Star.addConnection(
        getCurrentConnection()
    ));
    dispatch(Connection.reset())
}

export const commitISCToMap = () => {
    const dispatch = store.dispatch;
    dispatch(MapSettings.addInterStarConnection(
        getCurrentISC()
    ));
    dispatch(InterStarConnection.reset())
}

export const commitPlayerToMap = () => {
    const dispatch = store.dispatch;
    dispatch(MapSettings.addPlayer(
        getCurrentPlayer()
    ));
    dispatch(Player.reset())
}

export const commitGroupToTemplate = () => {
    const dispatch = store.dispatch;
    dispatch(Template.addGroup(
        getCurrentGroup()
    ));
    dispatch(Group.reset());
}

export const commitTemplateToMap = () => {
    const dispatch = store.dispatch;
    dispatch(MapSettings.addTemplate(
        getCurrentTemplate()
    ));
    dispatch(Template.reset())
}
