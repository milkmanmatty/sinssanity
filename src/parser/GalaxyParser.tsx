import { ConditionStartToken, ConditionTokens, ConnectionStartToken, ConnectionTokens, GroupStartToken, GroupTokens, InterStarConnectionStartToken, InterStarConnectionTokens, MapSettingTokens, MapStartToken, PlanetItemsStartToken, PlanetItemsTokens, PlanetStartToken, PlanetTokens, PlayerStartToken, PlayerTokens, StarStartToken, StarTokens, TemplateStartToken } from "./GalaxyReference";
import handlePlanetData from "./handlePlanetData";
import handleMapData from "./handleMapData";
import handleStarData from "./handleStarData";
import handleConditionData from "./handleConditionData";
import handleConnectionData from "./handleConnectionData";
import handleGroupData from "./handleGroupData";
import handlePlanetItemData from "./handlePlanetItemsData";
import handlePlayerData from "./handlePlayerData";
import handleTemplateData from "./handleTemplateData";
import * as save from "./commitEntities";
import * as cascade from "./scanSearchFunctions";
import handleInterStarConnectionData from "./handleInterStarConnectionData";
import { splitOnce } from "./helperFunctions";
import store from "../store";
import * as MapLoaded from "../reducers/mapLoadedReducer";

export interface GalaxyParserProps {
    galaxyLines: string[]
}

export type ParseSection =
    "Map" |
    "Star" |
    "Planet" |
    "PlanetItem" |
    "Group" |
    "Condition" |
    "Connection" |
    "InterStarConnection" |
    "Player" |
    "Template" |
    "TemplateGroup" |
    "TemplateCondition" |
    "NotFound"

const GalaxyParser = (props: GalaxyParserProps) => {
    let curStep: ParseSection = "Map";
    let hasPassedPlayer = false;
    const main = () => {
        for (let i = 0; i < props.galaxyLines.length; i++) {
            let l = props.galaxyLines[i].trim();

            //is line empty?
            if(l.length === 0){
                continue;
            }

            //see if we can split the line
            let prevStep = curStep;
            if(splitOnce(l,' ')[0] === l){
                if (identifySectionChange(l)) {
                    if(prevStep != curStep){
                        console.warn("Section change. Was "+prevStep+
                        " is now "+curStep);
                    }
                    continue;
                }
                console.error("Could not parse line: "+l+"\n"+
                    "Could not split line, delimiter not found");
                continue;
            }
            handleLineData(l);
        }
        store.dispatch(MapLoaded.setIsLoaded(true));
    }


    const identifySectionChange = (line: string) => {
        if (line === MapStartToken) {
            curStep = "Map";
            return true;

        } else if (line === StarStartToken) {
            if (curStep === "Star") {
                save.commitStarToMap();
            } else if (curStep === "Condition") {
                save.commitConditionToGroup();
                save.commitGroupToPlanetItem();
                save.commitPlanetItemToPlanet();
                save.commitPlanetToStar();
            } else if (curStep === "Group") {
                save.commitGroupToPlanetItem();
                save.commitPlanetItemToPlanet();
                save.commitPlanetToStar();
            } else if (curStep === "PlanetItem") {
                save.commitPlanetItemToPlanet();
                save.commitPlanetToStar();
            } else if (curStep === "Planet") {
                save.commitPlanetToStar();
            } else if (curStep === "Connection") {
                save.commitConnectionToStar();
            }
            curStep = "Star";
            return true;

        } else if (line === PlanetStartToken) {
            if (curStep === "Planet") {
                save.commitPlanetToStar();
            } else if (curStep === "PlanetItem") {
                save.commitPlanetItemToPlanet();
                save.commitPlanetToStar();
            } else if (curStep === "Group") {
                save.commitGroupToPlanetItem();
                save.commitPlanetItemToPlanet();
                save.commitPlanetToStar();
            } else if (curStep === "Condition") {
                save.commitConditionToGroup();
                save.commitGroupToPlanetItem();
                save.commitPlanetItemToPlanet();
                save.commitPlanetToStar();
            }
            curStep = "Planet";
            return true;

        } else if (line === PlanetItemsStartToken) {
            curStep = "PlanetItem";
            return true;

        } else if (line === GroupStartToken) {
            if (curStep === "TemplateGroup") {
                save.commitGroupToTemplate();
            } else if (curStep === "TemplateCondition") {
                save.commitConditionToGroup();
                save.commitGroupToTemplate();
            } else if (curStep === "Group") {
                save.commitGroupToPlanetItem();
            } else if (curStep === "Condition") {
                save.commitConditionToGroup();
                save.commitGroupToPlanetItem();
            }
            curStep = hasPassedPlayer ? "TemplateGroup" : "Group";
            return true;

        } else if (line === ConditionStartToken) {
            curStep = hasPassedPlayer ? "TemplateCondition" : "Condition";
            return true;

        } else if (line === ConnectionStartToken) {
            if (curStep === "Connection") {
                save.commitConnectionToStar();
            } else if (curStep === "Planet") {
                save.commitPlanetToStar();
            } else if (curStep === "PlanetItem") {
                save.commitPlanetItemToPlanet();
                save.commitPlanetToStar();
            } else if (curStep === "Group") {
                save.commitGroupToPlanetItem();
                save.commitPlanetItemToPlanet();
                save.commitPlanetToStar();
            } else if (curStep === "Condition") {
                save.commitConditionToGroup();
                save.commitGroupToPlanetItem();
                save.commitPlanetItemToPlanet();
                save.commitPlanetToStar();
            }
            curStep = "Connection";
            return true;

        } else if (line === InterStarConnectionStartToken) {
            if (curStep === "InterStarConnection") {
                save.commitISCToMap();
            }
            curStep = "InterStarConnection";
            return true;

        } else if (line === PlayerStartToken) {
            if (curStep === "Player") {
                save.commitPlayerToMap();
            }
            curStep = "Player";
            hasPassedPlayer = true;
            return true;

        } else if (hasPassedPlayer && line === TemplateStartToken) {
            curStep = "Template";
            return true;
        }
        return false;
    }


    const handleLineData = (line: string) => {
        let token = splitOnce(line,' ')[0];
        //Basic conditions
        if (curStep === "Map" && MapSettingTokens.includes(token)) {
            handleMapData(line);
        } else if (curStep === "Star" && StarTokens.includes(token)) {
            handleStarData(line);
        } else if (curStep === "Planet" && PlanetTokens.includes(token)) {
            handlePlanetData(line);
        } else if (curStep === "PlanetItem" && PlanetItemsTokens.includes(token)) {
            handlePlanetItemData(line);
        } else if (curStep === "Group" && GroupTokens.includes(token)) {
            handleGroupData(line);
        } else if (curStep === "Condition" && ConditionTokens.includes(token)) {
            handleConditionData(line);
        } else if (curStep === "Connection" && ConnectionTokens.includes(token)) {
            handleConnectionData(line);
        } else if (
            curStep === "InterStarConnection" &&
            InterStarConnectionTokens.includes(token)
        ) {
            handleInterStarConnectionData(line);
        } else if (curStep === "Player" && PlayerTokens.includes(token)) {
            handlePlayerData(line);
        }

        //Exit conditions
        //There are no uniform end tokens, guess and check.
        else if (curStep === "Condition") {
            console.log("Condition exit entered");
            save.commitConditionToGroup();
            if (GroupTokens.includes(token)) {
                curStep = "Group";
                handleGroupData(line);
            } else {
                scanForCorrectSection(line);
            }

        } else if (curStep === "Group") {
            console.log("Group exit entered");
            if (!hasPassedPlayer) {
                save.commitGroupToPlanetItem();
                if (PlanetItemsTokens.includes(token)) {
                    curStep = "PlanetItem";
                    handleGroupData(line);
                } else if (PlanetTokens.includes(token)) {
                    curStep = "Planet";
                    handleGroupData(line);
                }
            } else {
                save.commitGroupToTemplate();
                curStep = "Template";
                handleTemplateData(line);
            }
            scanForCorrectSection(line);

        } else if (curStep === "PlanetItem") {
            console.log("PlanetItem exit entered");
            save.commitPlanetItemToPlanet();
            if (PlanetTokens.includes(token)) {
                curStep = "Planet";
                handlePlanetData(line);
            } else if (StarTokens.includes(token)) {
                curStep = "Star";
                handleStarData(line);
            } else {
                scanForCorrectSection(line);
            }
        }

        else if (curStep === "Planet") {
            console.log("Planet exit entered");
            save.commitPlanetToStar();
            if (StarTokens.includes(token)) {
                curStep = "Star";
                handleStarData(line);
            } else {
                scanForCorrectSection(line);
            }
        }

        else if (curStep === "Connection") {
            console.log("Connection exit entered");
            if (StarTokens.includes(token)) {
                save.commitConnectionToStar();
                curStep = "Star";
                handleStarData(line);
            }
        }

        else {
            scanForCorrectSection(line);
        }
    }


    const scanForCorrectSection = (line: string) => {
        console.log("Scaning. CurStep ", curStep, " line", line);
        let token = splitOnce(line)[0];
        let found: ParseSection = "NotFound";
        //Search backwards
        switch (curStep) {
            case "Star":
            case "Player":
            case "Template":
                found = cascade.searchForMap(line, curStep);
                break;
            case "Planet":
                found = cascade.searchForConnection(line, curStep);
                break;
            case "PlanetItem":
                found = cascade.searchForPlanet(line, curStep);
                break;
            case "Group":
                found = cascade.searchForPlanetItems(line, curStep);
                break;
            case "Condition":
                found = cascade.searchForGroup(line, curStep);
                break;
            case "Connection":
                found = cascade.searchForStar(line, curStep);
                break;
            case "Map":
            default:
                break;
        }
        if (found !== "NotFound") {
            console.warn("scan curStep change. Was "+curStep+" now: "+found)
            curStep = found;
        } else {
            console.error("Could not handle line: " + line +
                "\nThe line is possibly out of order or custom data."+
                "\nToken: ["+token+"]\n"+
                "\nCurStep: "+curStep);
        }
    }
    main();
};

export default GalaxyParser;
