import store from "../store";
import { condition } from "../reducers/conditionReducer";
import { connection } from "../reducers/connectionReducer";
import { group } from "../reducers/groupReducer";
import { planet } from "../reducers/planetReducer";
import { player } from "../reducers/playerReducer";
import { pos, star } from "../reducers/starReducer";
import { template } from "../reducers/templateReducer";
import { interStarConnection } from "../reducers/interStarConnectionReducer";

/**
 * Splits the `line` string on the first occurance of `delimiter`
 * @param line the string to split
 * @param delimiter the character(s) to split on
 * @returns an array of length 2 containing the sections before
 * and after the delimiter respectfully. If no occurance of
 * `delimiter` is found then an array containing `line` is returned
 */
export const splitOnce = (line: string, delimiter = ' ') => {
    var index = line.indexOf(delimiter);
    if(index === -1){
        return [line];
    }
    return [
        line.slice(0,index),
        line.slice(index+delimiter.length)
    ];
}

/**
 * Removes double quotes from a string, leaves single quotes in place.
 * @param val string to remove double quotes from.
 * @returns `val` without any double quote characters contained within.
 */
export const rmq = (val: string) => {
    return val.replaceAll('"', '')
}

/**
 * Tries to parse a string as an int using radix (base) 10.
 * @param val input string to parse.
 * @param defaultVal Optional. What will be returned if `val` fails to parse.
 * 0 if not provided.
 * @returns parsed int of `val` or `defaultVal` if `val` failed to parse.
 */
export const tryParseInt = (val: string, defaultVal: number = 0) => {
    try {
        if (val.length > 0) {
            let result = parseInt(val, 10);
            if (!isNaN(result)) {
                return result;
            }
        }
    } catch {
        console.warn("tryParseInt could not parse input: " + val);
    }
    return defaultVal;
}

/**
 * Tries to parse a string as a float.
 * @param val input string to parse.
 * @param defaultVal Optional. What will be returned if `val` fails to parse.
 * 0 if not provided.
 * @returns parsed float of `val` or `defaultVal` if `val` failed to parse.
 */
export const tryParseFloat = (val: string, defaultVal: number = 0.0) => {
    try {
        if (val.length > 0) {
            let result = parseFloat(val);
            if (!isNaN(result)) {
                return result;
            }
        }
    } catch {
        console.warn("tryParseFloat could not parse input: " + val);
    }
    return defaultVal;
}

export const parsePos = (value: string) => {
    let result: pos = { x: 0, y: 0 };
    try {
        let thinValue = value.replaceAll("[", "").replaceAll("]", "").trim();
        let [x, y] = thinValue.split(',');

        //if parseInt results in NaN set to 0
        result = {
            x: (parseInt(x.trim(), 10) || 0),
            y: (parseInt(y.trim(), 10) || 0)
        }

    } catch {
        console.warn("Pos result is [0,0]. Could not parse pos value: ", value);
    }
    return result;
}


export const getCurrentStar = () => {
    const state = store.getState().starReducer;
    let r: star = {
        index: state.index,
        designName: state.designName,
        inGameName: state.inGameName,
        type: state.type,
        pos: state.pos,
        radius: state.radius,
        planetCount: state.planetCount,
        planetArray: state.planetArray,
        connectionCount: state.connectionCount,
        connectionArray: state.connectionArray,
        entityCount: state.entityCount,
        spawnProbability: state.spawnProbability
    }
    return r;
}
export const getCurrentPlanet = () => {
    const state = store.getState().planetReducer;
    let r: planet = {
        index: state.index,
        designName: state.designName,
        inGameName: state.inGameName,
        type: state.type,
        pos: state.pos,
        owner: state.owner,
        isHomePlanet: state.isHomePlanet,
        normalStartUpgradeLevelForPopulation:
            state.normalStartUpgradeLevelForPopulation,
        normalStartUpgradeLevelForCivilianModules:
            state.quickStartUpgradeLevelForCivilianModules,
        normalStartUpgradeLevelForTacticalModules:
            state.quickStartUpgradeLevelForTacticalModules,
        normalStartUpgradeLevelForArtifacts:
            state.normalStartUpgradeLevelForArtifacts,
        normalStartUpgradeLevelForInfrastructure:
            state.normalStartUpgradeLevelForInfrastructure,
        quickStartUpgradeLevelForPopulation:
            state.quickStartUpgradeLevelForPopulation,
        quickStartUpgradeLevelForCivilianModules:
            state.quickStartUpgradeLevelForCivilianModules,
        quickStartUpgradeLevelForTacticalModules:
            state.quickStartUpgradeLevelForTacticalModules,
        quickStartUpgradeLevelForArtifacts:
            state.quickStartUpgradeLevelForArtifacts,
        quickStartUpgradeLevelForInfrastructure:
            state.quickStartUpgradeLevelForInfrastructure,
        planetItems: state.planetItems,
        spawnProbability: state.spawnProbability,
        useDefaultTemplate: state.useDefaultTemplate,
        entityCount: state.entityCount,
        asteroidCount: state.asteroidCount
    }
    return r;
}
export const getCurrentPlanetItem = () => {
    const state = store.getState().planetItemsReducer;
    let r: template = {
        templateName: state.templateName,
        subTemplates: state.subTemplates,
        subTemplateArray: state.subTemplateArray,
        groups: state.groupArray.length,
        groupArray: state.groupArray
    }
    return r;
}
export const getCurrentGroup = () => {
    const state = store.getState().groupReducer;
    let r: group = {
        condition: state.condition,
        owner: state.owner,
        colonizeChance: state.colonizeChance,
        items: state.itemsArray.length,
        itemsArray: state.itemsArray
    }
    return r;
}
export const getCurrentCondition = () => {
    const state = store.getState().conditionReducer;
    let r: condition = {
        type: state.type,
        param: state.param
    }
    return r;
}
export const getCurrentConnection = () => {
    const state = store.getState().connectionReducer;
    let r: connection = {
        planetIndexA: state.planetIndexA,
        planetIndexB: state.planetIndexB,
        spawnProbability: state.spawnProbability,
        type: state.type
    }
    return r;
}
export const getCurrentISC = () => {
    const state = store.getState().interStarConnectionReducer;
    let r: interStarConnection = {
        starIndexA: state.starIndexA,
        planetIndexA: state.planetIndexA,
        starIndexB: state.starIndexB,
        planetIndexB: state.planetIndexB,
        spawnProbability: state.spawnProbability,
        type: state.type
    }
    return r;
}
export const getCurrentPlayer = () => {
    const state = store.getState().playerReducer;
    let r: player = {
        designName: state.designName,
        inGameName: state.inGameName,
        overrideRaceName: state.overrideRaceName,
        teamIndex: state.teamIndex,
        startingCredits: state.startingCredits,
        startingMetal: state.startingMetal,
        startingCrystal: state.startingCrystal,
        isNormalPlayer: state.isNormalPlayer,
        isRaidingPlayer: state.isRaidingPlayer,
        isInsurgentPlayer: state.isInsurgentPlayer,
        isOccupationPlayer: state.isOccupationPlayer,
        isMadVasariPlayer: state.isMadVasariPlayer,
        themeGroup: state.themeGroup,
        themeIndex: state.themeIndex,
        pictureGroup: state.pictureGroup,
        pictureIndex: state.pictureIndex
    }
    return r;
}
export const getCurrentTemplate = () => {
    const state = store.getState().templateReducer;
    let r: template = {
        templateName: state.templateName,
        subTemplates: state.subTemplates,
        subTemplateArray: state.subTemplateArray,
        groups: state.groupArray.length,
        groupArray: state.groupArray
    }
    return r;
}
