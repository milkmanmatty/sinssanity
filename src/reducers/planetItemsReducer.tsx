import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { group } from './groupReducer';
import { subtemplate, template } from './templateReducer';

// Define the initial state using that type
const initialState: template = {
    templateName: "",
    subTemplates: 0,
    subTemplateArray: [],
    groups: 0,
    groupArray: []
}

export const planetItemsSlice = createSlice({
    name: 'planetItemsSettings',
    initialState,
    reducers: {
        setTemplateName: (state, action: PayloadAction<string>) => {
            state.templateName = action.payload
        },
        setSubTemplates: (state, action: PayloadAction<number>) => {
            state.subTemplates = action.payload
        },
        addTemplate: (state, action: PayloadAction<subtemplate>) => {
            state.subTemplateArray = [...state.subTemplateArray , action.payload]
        },
        setGroups: (state, action: PayloadAction<number>) => {
            state.groups = action.payload
        },
        setGroupArray: (state, action: PayloadAction<group[]>) => {
            state.groupArray = action.payload
        },
        addGroup: (state, action: PayloadAction<group>) => {
            state.groupArray = [...state.groupArray , action.payload]
        },
        reset: () => initialState
    },
});

export const {
    setTemplateName,
    setSubTemplates,
    addTemplate,
    setGroups,
    setGroupArray,
    addGroup,
    reset
} = planetItemsSlice.actions;

export default planetItemsSlice.reducer;
