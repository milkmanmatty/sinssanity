import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { connection } from './connectionReducer';
import { planet } from './planetReducer';

export interface pos {
    x: number,
    y: number
}

export interface star {
    index: number,
    designName: string,
    inGameName: string,
    type: string,
    pos: pos,
    radius: number,
    planetCount: number,
    planetArray: planet[],
    connectionCount: number,
    connectionArray: connection[],
    entityCount: number,
    spawnProbability: number,
}

// Define the initial state using that type
const initialState: star = {
    index: 0,
    designName: "Star0",
    inGameName: "",
    type: "YellowStar",
    pos: { x: 150, y: 150 },
    radius: 100.000000,
    planetCount: 0,
    planetArray: [],
    connectionCount: 0,
    connectionArray: [],
    entityCount: 0,
    spawnProbability: 1.000000,
}

export const starSettingsSlice = createSlice({
    name: 'starSettings',
    initialState,
    reducers: {
        setIndex: (state, action: PayloadAction<number>) => {
            state.index = action.payload
        },
        setDesignName: (state, action: PayloadAction<string>) => {
            state.designName = action.payload
        },
        setInGameName: (state, action: PayloadAction<string>) => {
            state.inGameName = action.payload
        },
        setType: (state, action: PayloadAction<string>) => {
            state.type = action.payload
        },
        setPos: (state, action: PayloadAction<pos>) => {
            state.pos = action.payload
        },
        setPosX: (state, action: PayloadAction<number>) => {
            state.pos.x = action.payload
        },
        setPosY: (state, action: PayloadAction<number>) => {
            state.pos.y = action.payload
        },
        setPosUpdatePlanets: (state, action: PayloadAction<pos>) => {
            const difX = action.payload.x - state.pos.x;
            const difY = action.payload.y - state.pos.y;
            state.planetArray = state.planetArray.map((planet) => {
                planet.pos.x = planet.pos.x + difX;
                planet.pos.y = planet.pos.x + difY;
                return planet;
            });
            state.pos = {
                x: state.pos.x + difX,
                y: state.pos.y + difY
            };
        },
        setRadius: (state, action: PayloadAction<number>) => {
            state.radius = action.payload
        },
        setPlanetArray: (state, action: PayloadAction<planet[]>) => {
            state.planetArray = action.payload
        },
        addPlanet: (state, action: PayloadAction<planet>) => {
            state.planetArray = [...state.planetArray, action.payload];
        },
        setConnectionArray: (state, action: PayloadAction<connection[]>) => {
            state.connectionArray = action.payload
        },
        addConnection: (state, action: PayloadAction<connection>) => {
            state.connectionArray = [...state.connectionArray, action.payload];
        },
        setEntityCount: (state, action: PayloadAction<number>) => {
            state.entityCount = action.payload
        },
        setSpawnProbability: (state, action: PayloadAction<number>) => {
            state.spawnProbability = action.payload
        },
        reset: () => initialState,
        loadStar: (state, action: PayloadAction<star>) => {
            state.designName = action.payload.designName,
                state.inGameName = action.payload.inGameName;
            state.type = action.payload.type;
            state.pos = action.payload.pos;
            state.radius = action.payload.radius;
            state.planetCount = action.payload.planetCount;
            state.planetArray = action.payload.planetArray;
            state.connectionCount = action.payload.connectionCount;
            state.connectionArray = action.payload.connectionArray;
            state.entityCount = action.payload.entityCount;
            state.spawnProbability = action.payload.spawnProbability;
        }
    },
});

export const {
    setIndex,
    setDesignName,
    setInGameName,
    setType,
    setPos,
    setPosX,
    setPosY,
    setPosUpdatePlanets,
    setRadius,
    setPlanetArray,
    addPlanet,
    setConnectionArray,
    addConnection,
    setEntityCount,
    setSpawnProbability,
    reset,
    loadStar
} = starSettingsSlice.actions;

export default starSettingsSlice.reducer;
