import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../store';

export interface connection {
    planetIndexA: number,
    planetIndexB: number,
    spawnProbability: number,
    type: string
}

// Define the initial state using that type
const initialState: connection = {
    planetIndexA: 0,
    planetIndexB: 1,
    spawnProbability: 1.000000,
    type: "PhaseLane"
}

export const connectionSlice = createSlice({
    name: 'connectionSettings',
    initialState,
    reducers: {
        setPlanetIndexA: (state, action: PayloadAction<number>) => {
            state.planetIndexA = action.payload
        },
        setPlanetIndexB: (state, action: PayloadAction<number>) => {
            state.planetIndexB = action.payload
        },
        setSpawnProbability: (state, action: PayloadAction<number>) => {
            state.spawnProbability = action.payload
        },
        setType: (state, action: PayloadAction<string>) => {
            state.type = action.payload
        },
        reset: () => initialState,
        loadConnection: (state, action: PayloadAction<connection>) => {
            state = action.payload
        },
    },
});

export const {
    setPlanetIndexA,
    setPlanetIndexB,
    setSpawnProbability,
    setType,
    reset,
    loadConnection
} = connectionSlice.actions;

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
export const selectType = (state: RootState) => state.connectionReducer.type;

export default connectionSlice.reducer;
