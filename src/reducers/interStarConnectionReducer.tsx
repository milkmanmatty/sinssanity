import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface interStarConnection {
    starIndexA: number
    planetIndexA: number,
    starIndexB: number,
    planetIndexB: number,
    spawnProbability: number,
    type: string
}

// Define the initial state using that type
const initialState: interStarConnection = {
    starIndexA: 0,
    planetIndexA: 0,
    starIndexB: 1,
    planetIndexB: 1,
    spawnProbability: 1.000000,
    type: "Wormhole"
}

export const interStarConnectionSlice = createSlice({
    name: 'connectionSettings',
    initialState,
    reducers: {
        setStarIndexA: (state, action: PayloadAction<number>) => {
            state.starIndexA = action.payload
        },
        setPlanetIndexA: (state, action: PayloadAction<number>) => {
            state.planetIndexA = action.payload
        },
        setStarIndexB: (state, action: PayloadAction<number>) => {
            state.starIndexB = action.payload
        },
        setPlanetIndexB: (state, action: PayloadAction<number>) => {
            state.planetIndexB = action.payload
        },
        setSpawnProbability: (state, action: PayloadAction<number>) => {
            state.spawnProbability = action.payload
        },
        setType: (state, action: PayloadAction<string>) => {
            state.type = action.payload
        },
        reset: () => initialState
    },
});

export const {
    setStarIndexA,
    setPlanetIndexA,
    setStarIndexB,
    setPlanetIndexB,
    setSpawnProbability,
    setType,
    reset
} = interStarConnectionSlice.actions;

export default interStarConnectionSlice.reducer;
