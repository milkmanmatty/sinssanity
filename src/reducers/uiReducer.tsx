import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { connection } from './connectionReducer';
import { mapSettings } from './mapSettingsReducer';
import { planet } from './planetReducer';
import { pos, star } from './starReducer';

export type EntityString = 'MAP' |
'STAR' |
'PLANET' |
'CONNECTION' |
'UNKNOWN';

export type EntityType = mapSettings | star | planet | connection | undefined;

// Define a type for the slice state
export interface UIState {
    keyEvent: string,
    mouseEvent: string,
    cursor: string,
    curSelection: EntityString,
    curSelectionEntity: EntityType,
    viewportX: number,
    viewportY: number,
    scale: number,
    windowSize: pos
}

// Define the initial state using that type
const initialState: UIState = {
    keyEvent: '',
    mouseEvent: '',
    cursor: 'initial',
    curSelection: 'UNKNOWN',
    curSelectionEntity: undefined,
    viewportX: 0,
    viewportY: 0,
    scale: 3,
    windowSize: { x: 0, y: 0 }
}

export const uiSlice = createSlice({
    name: 'uiSize',
    initialState,
    reducers: {
        setKeyEvent: (state, action: PayloadAction<string>) => {
            state.keyEvent = action.payload;
        },
        setMouseEvent: (state, action: PayloadAction<string>) => {
            state.mouseEvent = action.payload;
        },
        setCursor: (state, action: PayloadAction<string>) => {
            state.cursor = action.payload;
        },
        setWindowSize: (state, action: PayloadAction<{x:number,y:number}>) => {
            state.windowSize = action.payload;
        },
        setViewportX: (state, action: PayloadAction<number>) => {
            state.viewportX = action.payload;
        },
        setViewportY: (state, action: PayloadAction<number>) => {
            state.viewportY = action.payload;
        },
        setScale: (state, action: PayloadAction<number>) => {
            state.scale = action.payload;
        },
        setSelection: (state, action: PayloadAction<EntityString>) => {
            state.curSelection = action.payload;
        },
        setSelectionEntity: (state, action: PayloadAction<EntityType>) => {
            state.curSelectionEntity = action.payload;
        },
    },
});

export const {
    setKeyEvent,
    setMouseEvent,
    setCursor,
    setWindowSize,
    setViewportX,
    setViewportY,
    setScale,
    setSelection,
    setSelectionEntity
} = uiSlice.actions;

export default uiSlice.reducer;
