import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface mapLoadedState {
    isLoaded: boolean
}
const initialState: mapLoadedState = {
    isLoaded: false
}
export const mapLoadedSlice = createSlice({
    name: 'mapLoadedSize',
    initialState,
    reducers: {
        setIsLoaded: (state, action: PayloadAction<boolean>) => {
            state.isLoaded = action.payload
        },
    },
});

export const { setIsLoaded } = mapLoadedSlice.actions;

export default mapLoadedSlice.reducer;
