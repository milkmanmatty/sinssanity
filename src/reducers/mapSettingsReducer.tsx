import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { interStarConnection } from './interStarConnectionReducer';
import { planet } from './planetReducer';
import { player } from './playerReducer';
import { star } from './starReducer';
import { template } from './templateReducer';

export interface recommendedGameType {
    recommendedGameType: string
}

export interface mapSettings {
    sinsArchiveVersion: number,
    versionNumber: number,
    isBrowsable: boolean,
    browsePictureName: string,
    browseName: string,
    browseDescription: string,
    isFirstCapitalShipIsFlagship: boolean,
    randomizeStartingPositions: boolean,
    planetArtifactDensity: number,
    planetBonusDensity: number,
    normalStartHomePlanetUpgradeLevel_Population: number,
    normalStartHomePlanetUpgradeLevel_CivilianModules: number,
    normalStartHomePlanetUpgradeLevel_TacticalModules: number,
    normalStartHomePlanetUpgradeLevel_Home: number,
    normalStartHomePlanetUpgradeLevel_ArtifactLevel: number,
    normalStartHomePlanetUpgradeLevel_Infrastructure: number,
    quickStartHomePlanetUpgradeLevel_Population: number,
    quickStartHomePlanetUpgradeLevel_CivilianModules: number,
    quickStartHomePlanetUpgradeLevel_TacticalModules: number,
    quickStartHomePlanetUpgradeLevel_Home: number,
    quickStartHomePlanetUpgradeLevel_ArtifactLevel: number,
    quickStartHomePlanetUpgradeLevel_Infrastructure: number,
    recommendedGameTypes: recommendedGameType[],
    metersPerGalaxyUnit: number,
    pixelsPerGalaxyUnit: number,
    useRandomGenerator: boolean,
    galaxyWidth: number,
    galaxyHeight: number,
    planetCount: number,
    playerStartingCredits: number,
    playerStartingMetal: number,
    playerStartingCrystal: number,
    starCount: number,
    starArray: star[],
    interStarConnectionCount: number,
    interStarConnectionArray: interStarConnection[],
    playerCount: number,
    playerArray: player[],
    template: number,
    templateArray: template[]
}

// Define the initial state using that type
const initialState: mapSettings = {
    sinsArchiveVersion: -1,
    versionNumber: 1.0,
    isBrowsable: true,
    browsePictureName: "",
    browseName: "New Sanity Map",
    browseDescription: "New Sanity Map",
    isFirstCapitalShipIsFlagship: true,
    randomizeStartingPositions: true,
    planetArtifactDensity: 0.15,
    planetBonusDensity: 0.4,
    normalStartHomePlanetUpgradeLevel_Population: 3,
    normalStartHomePlanetUpgradeLevel_CivilianModules: 1,
    normalStartHomePlanetUpgradeLevel_TacticalModules: 0,
    normalStartHomePlanetUpgradeLevel_Home: 1,
    normalStartHomePlanetUpgradeLevel_ArtifactLevel: 10,
    normalStartHomePlanetUpgradeLevel_Infrastructure: 2,
    quickStartHomePlanetUpgradeLevel_Population: 3,
    quickStartHomePlanetUpgradeLevel_CivilianModules: 3,
    quickStartHomePlanetUpgradeLevel_TacticalModules: 3,
    quickStartHomePlanetUpgradeLevel_Home: 1,
    quickStartHomePlanetUpgradeLevel_ArtifactLevel: 10,
    quickStartHomePlanetUpgradeLevel_Infrastructure: 3,
    recommendedGameTypes: [],
    metersPerGalaxyUnit: 25000.0,
    pixelsPerGalaxyUnit: 4.0,
    useRandomGenerator: false,
    galaxyWidth: 1920.0,
    galaxyHeight: 1080.0,
    planetCount: 0,
    playerStartingCredits: 1000,
    playerStartingMetal: 300,
    playerStartingCrystal: 100,
    starCount: 0,
    starArray: [],
    interStarConnectionCount: 0,
    interStarConnectionArray: [],
    playerCount: 0,
    playerArray: [],
    template: 0,
    templateArray: []
}

export const mapSettingsSlice = createSlice({
    name: 'mapSettings',
    initialState,
    reducers: {
        setSinsArchiveVersion: (state, action: PayloadAction<number>) => {
            state.sinsArchiveVersion = action.payload
        },
        setVersionNumber: (state, action: PayloadAction<number>) => {
            state.versionNumber = action.payload
        },
        setIsBrowsable: (state, action: PayloadAction<boolean>) => {
            state.isBrowsable = action.payload
        },
        setBrowsePictureName: (state, action: PayloadAction<string>) => {
            state.browsePictureName = action.payload
        },
        setBrowseName: (state, action: PayloadAction<string>) => {
            state.browseName = action.payload
        },
        setBrowseDescription: (state, action: PayloadAction<string>) => {
            state.browseDescription = action.payload
        },
        setIsFirstCapitalShipIsFlagship: (state, action: PayloadAction<boolean>) => {
            state.isFirstCapitalShipIsFlagship = action.payload
        },
        setRandomizeStartingPositions: (state, action: PayloadAction<boolean>) => {
            state.randomizeStartingPositions = action.payload
        },
        setPlanetArtifactDensity: (state, action: PayloadAction<number>) => {
            state.planetArtifactDensity = action.payload
        },
        setPlanetBonusDensity: (state, action: PayloadAction<number>) => {
            state.planetBonusDensity = action.payload
        },
        setNormal_Population: (state, action: PayloadAction<number>) => {
            state.normalStartHomePlanetUpgradeLevel_Population = action.payload
        },
        setNormal_CivilianModules: (state, action: PayloadAction<number>) => {
            state.normalStartHomePlanetUpgradeLevel_CivilianModules = action.payload
        },
        setNormal_TacticalModules: (state, action: PayloadAction<number>) => {
            state.normalStartHomePlanetUpgradeLevel_TacticalModules = action.payload
        },
        setNormal_Home: (state, action: PayloadAction<number>) => {
            state.normalStartHomePlanetUpgradeLevel_Home = action.payload
        },
        setNormal_ArtifactLevel: (state, action: PayloadAction<number>) => {
            state.normalStartHomePlanetUpgradeLevel_ArtifactLevel = action.payload
        },
        setNormal_Infrastructure: (state, action: PayloadAction<number>) => {
            state.normalStartHomePlanetUpgradeLevel_Infrastructure = action.payload
        },
        setQuick_Population: (state, action: PayloadAction<number>) => {
            state.quickStartHomePlanetUpgradeLevel_Population = action.payload
        },
        setQuick_CivilianModules: (state, action: PayloadAction<number>) => {
            state.quickStartHomePlanetUpgradeLevel_CivilianModules = action.payload
        },
        setQuick_TacticalModules: (state, action: PayloadAction<number>) => {
            state.quickStartHomePlanetUpgradeLevel_TacticalModules = action.payload
        },
        setQuick_Home: (state, action: PayloadAction<number>) => {
            state.quickStartHomePlanetUpgradeLevel_Home = action.payload
        },
        setQuick_ArtifactLevel: (state, action: PayloadAction<number>) => {
            state.quickStartHomePlanetUpgradeLevel_ArtifactLevel = action.payload
        },
        setQuick_Infrastructure: (state, action: PayloadAction<number>) => {
            state.quickStartHomePlanetUpgradeLevel_Infrastructure = action.payload
        },
        setRecommendedGameTypes: (state, action: PayloadAction<recommendedGameType[]>) => {
            state.recommendedGameTypes = action.payload
        },
        addRecommendedGameType: (state, action: PayloadAction<recommendedGameType>) => {
            state.recommendedGameTypes = [...state.recommendedGameTypes, action.payload]
        },
        setMetersPerGalaxyUnit: (state, action: PayloadAction<number>) => {
            state.metersPerGalaxyUnit = action.payload
        },
        setPixelsPerGalaxyUnit: (state, action: PayloadAction<number>) => {
            state.pixelsPerGalaxyUnit = action.payload
        },
        setUseRandomGenerator: (state, action: PayloadAction<boolean>) => {
            state.useRandomGenerator = action.payload
        },
        setGalaxyWidth: (state, action: PayloadAction<number>) => {
            state.galaxyWidth = action.payload
        },
        setGalaxyHeight: (state, action: PayloadAction<number>) => {
            state.galaxyHeight = action.payload
        },
        setPlayerStartingCredits: (state, action: PayloadAction<number>) => {
            state.playerStartingCredits = action.payload
        },
        setPlayerStartingMetal: (state, action: PayloadAction<number>) => {
            state.playerStartingMetal = action.payload
        },
        setPlayerStartingCrystal: (state, action: PayloadAction<number>) => {
            state.playerStartingCrystal = action.payload
        },
        setStars: (state, action: PayloadAction<star[]>) => {
            state.starArray = action.payload
        },
        addStar: (state, action: PayloadAction<star>) => {
            state.starArray = [...state.starArray, action.payload]
        },
        updateStar: (state, action: PayloadAction<star>) => {
            state.starArray = state.starArray.map((star) => {
                if (star.index === action.payload.index) {
                    return action.payload;
                }
                return star;
            });
        },
        setPlanetCount: (state, action: PayloadAction<number>) => {
            state.planetCount = action.payload
        },
        incrementPlanetCount: (state) => {
            state.planetCount += 1
        },
        updatePlanet: (state, action: PayloadAction<planet>) => {
            state.starArray = state.starArray.map((star) => {
                star.planetArray = star.planetArray.map((planet) => {
                    if (planet.index === action.payload.index) {
                        return action.payload;
                    }
                    return planet;
                })
                return star;
            });
        },
        addInterStarConnection: (state, action: PayloadAction<interStarConnection>) => {
            state.interStarConnectionArray = [
                ...state.interStarConnectionArray,
                action.payload
            ]
        },
        addPlayer: (state, action: PayloadAction<player>) => {
            state.playerArray = [...state.playerArray, action.payload]
        },
        addTemplate: (state, action: PayloadAction<template>) => {
            state.templateArray = [...state.templateArray, action.payload]
        },
        reset: () => initialState,
    },
});

export const {
    setSinsArchiveVersion,
    setVersionNumber,
    setIsBrowsable,
    setBrowsePictureName,
    setBrowseName,
    setBrowseDescription,
    setIsFirstCapitalShipIsFlagship,
    setRandomizeStartingPositions,
    setPlanetArtifactDensity,
    setPlanetBonusDensity,
    setNormal_Population,
    setNormal_CivilianModules,
    setNormal_TacticalModules,
    setNormal_Home,
    setNormal_ArtifactLevel,
    setNormal_Infrastructure,
    setQuick_Population,
    setQuick_CivilianModules,
    setQuick_TacticalModules,
    setQuick_Home,
    setQuick_ArtifactLevel,
    setQuick_Infrastructure,
    setRecommendedGameTypes,
    addRecommendedGameType,
    setMetersPerGalaxyUnit,
    setPixelsPerGalaxyUnit,
    setUseRandomGenerator,
    setGalaxyWidth,
    setGalaxyHeight,
    setPlayerStartingCredits,
    setPlayerStartingMetal,
    setPlayerStartingCrystal,
    setStars,
    addStar,
    updateStar,
    setPlanetCount,
    incrementPlanetCount,
    updatePlanet,
    addInterStarConnection,
    addPlayer,
    addTemplate,
    reset
} = mapSettingsSlice.actions;

export default mapSettingsSlice.reducer;
