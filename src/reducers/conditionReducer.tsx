import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface condition {
    type: string,
    param: string
}
export interface item {
    item: string
}

// Define the initial state using that type
const initialState: condition = {
    type: "",
    param: ""
}

export const conditionSlice = createSlice({
    name: 'conditionSettings',
    initialState,
    reducers: {
        setType: (state, action: PayloadAction<string>) => {
            state.type = action.payload
        },
        setParam: (state, action: PayloadAction<string>) => {
            state.param = action.payload
        },
        reset: () => initialState
    },
});

export const {
    setType,
    setParam,
    reset
} = conditionSlice.actions;

export default conditionSlice.reducer;
