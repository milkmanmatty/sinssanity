import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../store';

// Define a type for the slice state
export interface DetailsSizeState {
    value: number
}

// Define the initial state using that type
const initialState: DetailsSizeState = {
    value: 250
}

export const detailsSizeSlice = createSlice({
    name: 'detailsSize',
    initialState,
    reducers: {
        setToAmount: (state, action: PayloadAction<number>) => {
            state.value = action.payload
        },
    },
});

export const { setToAmount } = detailsSizeSlice.actions;

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
export const selectCount = (state: RootState) => state.detailsSize.value;

export default detailsSizeSlice.reducer;
