import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { condition } from './conditionReducer';

export interface group {
    condition: condition,
    owner: string,
    colonizeChance: number,
    items: number,
    itemsArray: item[],
}
export interface item {
    item: string
}

// Define the initial state using that type
const initialState: group = {
    condition: {
        type: "",
        param: ""
    },
    owner: "",
    colonizeChance: 0,
    items: 0,
    itemsArray: [],
}

export const groupSlice = createSlice({
    name: 'groupSettings',
    initialState,
    reducers: {
        setCondition: (state, action: PayloadAction<condition>) => {
            state.condition = action.payload
        },
        setOwner: (state, action: PayloadAction<string>) => {
            state.owner = action.payload
        },
        setColonizeChance: (state, action: PayloadAction<number>) => {
            state.colonizeChance = action.payload
        },
        setItems: (state, action: PayloadAction<number>) => {
            state.items = action.payload
        },
        setItemsArray: (state, action: PayloadAction<item[]>) => {
            state.itemsArray = action.payload
        },
        addItem: (state, action: PayloadAction<item>) => {
            state.itemsArray = [...state.itemsArray, action.payload];
            state.items = state.itemsArray.length;
        },
        reset: () => initialState
    },
});

export const {
    setCondition,
    setOwner,
    setColonizeChance,
    setItems,
    setItemsArray,
    addItem,
    reset
} = groupSlice.actions;

export default groupSlice.reducer;
