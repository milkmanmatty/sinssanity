import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../store';

export interface player {
    designName: string,
	inGameName: string,
	overrideRaceName: string,
	teamIndex: number,
	startingCredits: number,
	startingMetal: number,
	startingCrystal: number,
	isNormalPlayer: boolean,
	isRaidingPlayer: boolean,
	isInsurgentPlayer: boolean,
	isOccupationPlayer: boolean,
	isMadVasariPlayer: boolean,
	themeGroup: string,
	themeIndex: number,
	pictureGroup: string,
	pictureIndex: number
}

// Define the initial state using that type
const initialState: player = {
    designName: "NewPlayer",
	inGameName: "NewPlayer",
	overrideRaceName: "",
	teamIndex: -1,
	startingCredits: 3000,
	startingMetal: 800,
	startingCrystal: 250,
	isNormalPlayer: true,
	isRaidingPlayer: false,
	isInsurgentPlayer: false,
	isOccupationPlayer: false,
	isMadVasariPlayer: false,
	themeGroup: "",
	themeIndex: 0,
	pictureGroup: "",
	pictureIndex: 0
}

export const playerSlice = createSlice({
    name: 'playerSettings',
    initialState,
    reducers: {
        setDesignName: (state, action: PayloadAction<string>) => {
            state.designName = action.payload
        },
        setInGameName: (state, action: PayloadAction<string>) => {
            state.inGameName = action.payload
        },
        setOverrideRaceName: (state, action: PayloadAction<string>) => {
            state.overrideRaceName = action.payload
        },
        setTeamIndex: (state, action: PayloadAction<number>) => {
            state.teamIndex = action.payload
        },
        setStartingCredits: (state, action: PayloadAction<number>) => {
            state.startingCredits = action.payload
        },
        setStartingMetal: (state, action: PayloadAction<number>) => {
            state.startingMetal = action.payload
        },
        setStartingCrystal: (state, action: PayloadAction<number>) => {
            state.startingCrystal = action.payload
        },
        setIsNormalPlayer: (state, action: PayloadAction<boolean>) => {
            state.isNormalPlayer = action.payload
        },
        setIsRaidingPlayer: (state, action: PayloadAction<boolean>) => {
            state.isRaidingPlayer = action.payload
        },
        setIsInsurgentPlayer: (state, action: PayloadAction<boolean>) => {
            state.isInsurgentPlayer = action.payload
        },
        setIsOccupationPlayer: (state, action: PayloadAction<boolean>) => {
            state.isOccupationPlayer = action.payload
        },
        setIsMadVasariPlayer: (state, action: PayloadAction<boolean>) => {
            state.isMadVasariPlayer = action.payload
        },
        setThemeGroup: (state, action: PayloadAction<string>) => {
            state.themeGroup = action.payload
        },
        setThemeIndex: (state, action: PayloadAction<number>) => {
            state.themeIndex = action.payload
        },
        setPictureGroup: (state, action: PayloadAction<string>) => {
            state.pictureGroup = action.payload
        },
        setPictureIndex: (state, action: PayloadAction<number>) => {
            state.pictureIndex = action.payload
        },
        reset: () => initialState
    },
});

export const {
    setDesignName,
    setInGameName,
    setOverrideRaceName,
    setTeamIndex,
    setStartingCredits,
    setStartingMetal,
    setStartingCrystal,
    setIsNormalPlayer,
    setIsRaidingPlayer,
    setIsInsurgentPlayer,
    setIsOccupationPlayer,
    setIsMadVasariPlayer,
    setThemeGroup,
    setThemeIndex,
    setPictureGroup,
    setPictureIndex,
    reset
} = playerSlice.actions;

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
export const selectCount = (state: RootState) => state.playerReducer.designName;

export default playerSlice.reducer;
