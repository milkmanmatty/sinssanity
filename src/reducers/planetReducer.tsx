import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../store';
import { pos } from './starReducer';
import { template } from './templateReducer';

export interface planet {
    index: number,
    designName: string,
    inGameName: string,
    type: string,
    pos: pos,
    owner: string,
    isHomePlanet: boolean,
    normalStartUpgradeLevelForPopulation: number,
    normalStartUpgradeLevelForCivilianModules: number,
    normalStartUpgradeLevelForTacticalModules: number,
    normalStartUpgradeLevelForArtifacts: number,
    normalStartUpgradeLevelForInfrastructure: number,
    quickStartUpgradeLevelForPopulation: number,
    quickStartUpgradeLevelForCivilianModules: number,
    quickStartUpgradeLevelForTacticalModules: number,
    quickStartUpgradeLevelForArtifacts: number,
    quickStartUpgradeLevelForInfrastructure: number,
    planetItems: template,
    spawnProbability: number,
    useDefaultTemplate: boolean,
    entityCount: number,
    asteroidCount: number,
}

// Define the initial state using that type
const initialState: planet = {
    index: 0,
    designName: "Planet0",
    inGameName: "",
    type: "Terran",
    pos: { x: 0, y: 0 },
    owner: "",
    isHomePlanet: false,
    normalStartUpgradeLevelForPopulation: 3,
    normalStartUpgradeLevelForCivilianModules: 1,
    normalStartUpgradeLevelForTacticalModules: 0,
    normalStartUpgradeLevelForArtifacts: 10,
    normalStartUpgradeLevelForInfrastructure: 2,
    quickStartUpgradeLevelForPopulation: 4,
    quickStartUpgradeLevelForCivilianModules: 1,
    quickStartUpgradeLevelForTacticalModules: 0,
    quickStartUpgradeLevelForArtifacts: 10,
    quickStartUpgradeLevelForInfrastructure: 2,
    planetItems: {
        templateName: "",
        subTemplates: 0,
        subTemplateArray: [],
        groups: 0,
        groupArray: []
    },
    spawnProbability: 1.000000,
    useDefaultTemplate: true,
    entityCount: 0,
    asteroidCount: 0,
}

export const planetSlice = createSlice({
    name: 'planetSettings',
    initialState,
    reducers: {
        setIndex: (state, action: PayloadAction<number>) => {
            state.index = action.payload
        },
        setDesignName: (state, action: PayloadAction<string>) => {
            state.designName = action.payload
        },
        setInGameName: (state, action: PayloadAction<string>) => {
            state.inGameName = action.payload
        },
        setType: (state, action: PayloadAction<string>) => {
            state.type = action.payload
        },
        setPos: (state, action: PayloadAction<pos>) => {
            state.pos = action.payload
        },
        setPosX: (state, action: PayloadAction<number>) => {
            state.pos.x = action.payload
        },
        setPosY: (state, action: PayloadAction<number>) => {
            state.pos.y = action.payload
        },
        setOwner: (state, action: PayloadAction<string>) => {
            state.owner = action.payload
        },
        setIsHomePlanet: (state, action: PayloadAction<boolean>) => {
            state.isHomePlanet = action.payload
        },
        setNormal_Population: (state, action: PayloadAction<number>) => {
            state.normalStartUpgradeLevelForPopulation = action.payload
        },
        setNormal_CivilianModules: (state, action: PayloadAction<number>) => {
            state.normalStartUpgradeLevelForCivilianModules = action.payload
        },
        setNormal_TacticalModules: (state, action: PayloadAction<number>) => {
            state.normalStartUpgradeLevelForTacticalModules = action.payload
        },
        setNormal_Artifacts: (state, action: PayloadAction<number>) => {
            state.normalStartUpgradeLevelForArtifacts = action.payload
        },
        setNormal_Infrastructure: (state, action: PayloadAction<number>) => {
            state.normalStartUpgradeLevelForInfrastructure = action.payload
        },
        setQuick_Population: (state, action: PayloadAction<number>) => {
            state.quickStartUpgradeLevelForPopulation = action.payload
        },
        setQuick_CivilianModules: (state, action: PayloadAction<number>) => {
            state.quickStartUpgradeLevelForCivilianModules = action.payload
        },
        setQuick_TacticalModules: (state, action: PayloadAction<number>) => {
            state.quickStartUpgradeLevelForTacticalModules = action.payload
        },
        setQuick_Artifacts: (state, action: PayloadAction<number>) => {
            state.quickStartUpgradeLevelForArtifacts = action.payload
        },
        setQuick_Infrastructure: (state, action: PayloadAction<number>) => {
            state.quickStartUpgradeLevelForInfrastructure = action.payload
        },
        setPlanetItems: (state, action: PayloadAction<template>) => {
            state.planetItems = action.payload
        },
        setSpawnProbability: (state, action: PayloadAction<number>) => {
            state.spawnProbability = action.payload
        },
        setUseDefaultTemplate: (state, action: PayloadAction<boolean>) => {
            state.useDefaultTemplate = action.payload
        },
        setEntityCount: (state, action: PayloadAction<number>) => {
            state.entityCount = action.payload
        },
        setAsteroidCount: (state, action: PayloadAction<number>) => {
            state.asteroidCount = action.payload
        },
        reset: () => initialState,
        loadPlanet: (state, action: PayloadAction<planet>) => {
            state.index = action.payload.index;
            state.designName = action.payload.designName;
            state.inGameName = action.payload.inGameName;
            state.type = action.payload.type;
            state.pos = action.payload.pos;
            state.owner = action.payload.owner;
            state.isHomePlanet = action.payload.isHomePlanet;
            state.normalStartUpgradeLevelForPopulation = action.payload.normalStartUpgradeLevelForPopulation;
            state.normalStartUpgradeLevelForCivilianModules = action.payload.normalStartUpgradeLevelForCivilianModules;
            state.normalStartUpgradeLevelForTacticalModules = action.payload.normalStartUpgradeLevelForTacticalModules;
            state.normalStartUpgradeLevelForArtifacts = action.payload.normalStartUpgradeLevelForArtifacts;
            state.normalStartUpgradeLevelForInfrastructure = action.payload.normalStartUpgradeLevelForInfrastructure;
            state.quickStartUpgradeLevelForPopulation = action.payload.quickStartUpgradeLevelForPopulation;
            state.quickStartUpgradeLevelForCivilianModules = action.payload.quickStartUpgradeLevelForCivilianModules;
            state.quickStartUpgradeLevelForTacticalModules = action.payload.quickStartUpgradeLevelForTacticalModules;
            state.quickStartUpgradeLevelForArtifacts = action.payload.quickStartUpgradeLevelForArtifacts;
            state.quickStartUpgradeLevelForInfrastructure = action.payload.quickStartUpgradeLevelForInfrastructure;
            state.planetItems = action.payload.planetItems;
            state.spawnProbability = action.payload.spawnProbability;
            state.useDefaultTemplate = action.payload.useDefaultTemplate;
            state.entityCount = action.payload.entityCount;
            state.asteroidCount = action.payload.asteroidCount;
            console.log('changing planet '+state.designName);
        }
    },
});

export const {
    setIndex,
    setDesignName,
    setInGameName,
    setType,
    setPos,
    setPosX,
    setPosY,
    setOwner,
    setIsHomePlanet,
    setNormal_Population,
    setNormal_CivilianModules,
    setNormal_TacticalModules,
    setNormal_Artifacts,
    setNormal_Infrastructure,
    setQuick_Population,
    setQuick_CivilianModules,
    setQuick_TacticalModules,
    setQuick_Artifacts,
    setQuick_Infrastructure,
    setPlanetItems,
    setSpawnProbability,
    setUseDefaultTemplate,
    setEntityCount,
    setAsteroidCount,
    reset,
    loadPlanet
} = planetSlice.actions;

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
export const selectCount = (state: RootState) => state.planetReducer.type;

export default planetSlice.reducer;
