import React from 'react';
import { useAppSelector } from '../../hooks/hooks';
import {
    setDesignName,
    setInGameName,
    setType,
    setPosX,
    setPosY,
    setRadius,
//    setEntityCount,
    setSpawnProbability,
} from '../../reducers/starReducer';
import { displayTextField, displayPosField, displayNumberField } from './UIElements';

const StarEdit = () => {
    const state = useAppSelector(state => state.starReducer);

    return (
        <form className="edit-star-form">
            {displayTextField('Design Name', state.designName, setDesignName)}
            {displayTextField('In Game Name', state.inGameName, setInGameName)}
            {displayTextField('Type', state.type, setType)}
            {displayPosField(
                ['Star position', 'X', 'Y'],
                state.pos,
                [setPosX, setPosY]
            )}
            {displayNumberField('Radius', state.radius, setRadius, false)}
            {displayNumberField(
                'Spawn Probability',
                state.spawnProbability,
                setSpawnProbability,
                false
            )}
        </form>
    );
};

export default StarEdit;
