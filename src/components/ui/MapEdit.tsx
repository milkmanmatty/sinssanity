import React from 'react';
import { useAppSelector } from '../../hooks/hooks';
import {
    setBrowseDescription,
    setBrowseName,
    setBrowsePictureName,
    setGalaxyHeight,
    setGalaxyWidth,
    setIsBrowsable,
    setIsFirstCapitalShipIsFlagship,
    setNormal_ArtifactLevel,
    setNormal_CivilianModules,
    setNormal_Home,
    setNormal_Infrastructure,
    setNormal_Population,
    setNormal_TacticalModules,
    setPlanetArtifactDensity,
    setPlanetBonusDensity,
    setQuick_ArtifactLevel,
    setQuick_CivilianModules,
    setQuick_Home,
    setQuick_Infrastructure,
    setQuick_Population,
    setQuick_TacticalModules,
    setRandomizeStartingPositions,
    setUseRandomGenerator
} from '../../reducers/mapSettingsReducer';
import { displayBoolField, displayTextField, displayTextArea, displayNumberField, displaySeparator } from './UIElements';

const MapEdit = () => {
    const state = useAppSelector(state => state.mapSettings);

    return (
        <form className="edit-map-form">
            <button onClick={() => {
                console.log(state);
            }} type="button">i</button>
            {displayBoolField('is Browsable?', 'isBrowsable', state.isBrowsable, setIsBrowsable)}
            {displayTextField('Browse Picture Name', state.browsePictureName, setBrowsePictureName)}
            {displayTextField('Browse Name', state.browseName, setBrowseName)}
            {displayTextArea('Browse Description', state.browseDescription, setBrowseDescription)}
            {displayBoolField(
                'is First Capital Ship A Flagship?',
                'isFirstCapitalShipIsFlagship',
                state.isFirstCapitalShipIsFlagship,
                setIsFirstCapitalShipIsFlagship
            )}
            {displayBoolField(
                'Randomize Starting Positions?',
                'randomizeStartingPositions',
                state.randomizeStartingPositions,
                setRandomizeStartingPositions
            )}
            {displayNumberField('Planet Artifact Density', state.planetArtifactDensity, setPlanetArtifactDensity, false)}
            {displayNumberField('Planet Bonus Density', state.planetBonusDensity, setPlanetBonusDensity, false)}
            {displaySeparator('Normal Start Options:')}
            {displayNumberField('Population Level',
                state.normalStartHomePlanetUpgradeLevel_Population,
                setNormal_Population, true)}
            {displayNumberField('Civilian Modules Level',
                state.normalStartHomePlanetUpgradeLevel_CivilianModules,
                setNormal_CivilianModules, true)}
            {displayNumberField('Tactical Modules Level',
                state.normalStartHomePlanetUpgradeLevel_TacticalModules,
                setNormal_TacticalModules, true)}
            {displayNumberField('Home Level',
                state.normalStartHomePlanetUpgradeLevel_Home,
                setNormal_Home, true)}
            {displayNumberField('Artifact Level',
                state.normalStartHomePlanetUpgradeLevel_ArtifactLevel,
                setNormal_ArtifactLevel, true)}
            {displayNumberField('Infrastructure Level',
                state.normalStartHomePlanetUpgradeLevel_Infrastructure,
                setNormal_Infrastructure, true)}
            {displaySeparator('Quick Start Options:')}
            {displayNumberField('Population Level',
                state.quickStartHomePlanetUpgradeLevel_Population,
                setQuick_Population, true)}
            {displayNumberField('Civilian Modules Level',
                state.quickStartHomePlanetUpgradeLevel_CivilianModules,
                setQuick_CivilianModules, true)}
            {displayNumberField('Tactical Modules Level',
                state.quickStartHomePlanetUpgradeLevel_TacticalModules,
                setQuick_TacticalModules, true)}
            {displayNumberField('Home Level',
                state.quickStartHomePlanetUpgradeLevel_Home,
                setQuick_Home, true)}
            {displayNumberField('Artifact Level',
                state.quickStartHomePlanetUpgradeLevel_ArtifactLevel,
                setQuick_ArtifactLevel, true)}
            {displayNumberField('Infrastructure Level',
                state.quickStartHomePlanetUpgradeLevel_Infrastructure,
                setQuick_Infrastructure, true)}
            {displayBoolField(
                'Use Random Generator?',
                'UseRandomGenerator',
                state.useRandomGenerator,
                setUseRandomGenerator
            )}
            {displayNumberField('Galaxy Width', state.galaxyWidth, setGalaxyWidth, false)}
            {displayNumberField('Galaxy Height', state.galaxyHeight, setGalaxyHeight, false)}
        </form>
    );
};

export default MapEdit;
