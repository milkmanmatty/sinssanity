import React from 'react';
import { useAppSelector } from '../../hooks/hooks';
import {
    setDesignName,
    setInGameName,
    setType,
    setPosX,
    setPosY,
    setOwner,
    setIsHomePlanet,
    setNormal_Population,
    setNormal_CivilianModules,
    setNormal_TacticalModules,
    setNormal_Artifacts,
    setNormal_Infrastructure,
    setQuick_Population,
    setQuick_CivilianModules,
    setQuick_TacticalModules,
    setQuick_Artifacts,
    setQuick_Infrastructure,
    //    setPlanetItems,
    setSpawnProbability,
    setUseDefaultTemplate,
    setEntityCount,
    setAsteroidCount
} from '../../reducers/planetReducer';
import { displayBoolField, displayNumberField, displayPosField, displaySeparator, displayTextField } from './UIElements';

const PlanetEdit = () => {
    let state = useAppSelector(state => state.planetReducer);

    return (
        <form className="edit-planet-form">
            {displayTextField('Design Name', state.designName, setDesignName)}
            {displayTextField('In Game Name', state.inGameName, setInGameName)}
            {displayTextField('Type', state.type, setType)}
            {displayPosField(
                ['Planet position', 'X', 'Y'],
                state.pos,
                [setPosX, setPosY]
            )}
            {displayTextField('Owner', state.owner, setOwner)}
            {displayBoolField(
                'Is Home Planet?',
                'isHomePlanet',
                state.isHomePlanet,
                setIsHomePlanet
            )}
            {displaySeparator('Normal Start Options:')}
            {displayNumberField('Population Level',
                state.normalStartUpgradeLevelForPopulation,
                setNormal_Population, true)}
            {displayNumberField('Civilian Modules Level',
                state.normalStartUpgradeLevelForCivilianModules,
                setNormal_CivilianModules, true)}
            {displayNumberField('Tactical Modules Level',
                state.normalStartUpgradeLevelForTacticalModules,
                setNormal_TacticalModules, true)}
            {displayNumberField('Artifact Level',
                state.normalStartUpgradeLevelForArtifacts,
                setNormal_Artifacts, true)}
            {displayNumberField('Infrastructure Level',
                state.normalStartUpgradeLevelForInfrastructure,
                setNormal_Infrastructure, true)}

            {displaySeparator('Quick Start Options:')}
            {displayNumberField('Population Level',
                state.quickStartUpgradeLevelForPopulation,
                setQuick_Population, true)}
            {displayNumberField('Civilian Modules Level',
                state.quickStartUpgradeLevelForCivilianModules,
                setQuick_CivilianModules, true)}
            {displayNumberField('Tactical Modules Level',
                state.quickStartUpgradeLevelForTacticalModules,
                setQuick_TacticalModules, true)}
            {displayNumberField('Artifact Level',
                state.quickStartUpgradeLevelForArtifacts,
                setQuick_Artifacts, true)}
            {displayNumberField('Infrastructure Level',
                state.quickStartUpgradeLevelForInfrastructure,
                setQuick_Infrastructure, true)}

            {displayNumberField(
                'Spawn Probability',
                state.spawnProbability,
                setSpawnProbability,
                false
            )}
            {displayBoolField(
                'Use Default Template?',
                'useDefaultTemplate',
                state.useDefaultTemplate,
                setUseDefaultTemplate
            )}
            {displayNumberField('Entity Count',
                state.entityCount,
                setEntityCount, true)}
            {displayNumberField('Asteroid Count',
                state.asteroidCount,
                setAsteroidCount, true)}
        </form>
    );
};

export default PlanetEdit;
