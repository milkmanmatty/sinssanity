import React from 'react';
import { useAppSelector } from '../../hooks/hooks';
import {
    setSpawnProbability,
    setType
} from '../../reducers/connectionReducer';
import { displayLabel, displayTextField, displayNumberField } from './UIElements';

const StarEdit = () => {
    const state = useAppSelector(state => state.connectionReducer);

    return (
        <form className="edit-connection-form">
            {displayLabel('Planet Index A', ''+state.planetIndexA)}
            {displayLabel('Planet Index B', ''+state.planetIndexB)}
            {displayTextField('Type', state.type, setType)}
            {displayNumberField(
                'Spawn Probability',
                state.spawnProbability,
                setSpawnProbability,
                false
            )}
        </form>
    );
};

export default StarEdit;
