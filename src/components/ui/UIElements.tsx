import React from "react";
import { pos } from "../../reducers/starReducer";
import store from "../../store";

export const displayTextArea = (
    label: string,
    defaultValue: string,
    action: any
) => {
    return (
        <label>
            {label}
            <br />
            <textarea
                defaultValue={defaultValue}
                onChange={(e) => {
                    store.dispatch(action(e.currentTarget.value));
                }}
            />
        </label>
    )
}

export const displayTextField = (
    label: string,
    defaultValue: string,
    action: any
) => {
    return (
        <label>
            {label}
            <br />
            <input
                type="text"
                value={defaultValue}
                onChange={(e) => {
                    store.dispatch(action(e.currentTarget.value));
                }}
            />
        </label>
    )
}

export const displayLabel = (
    label: string,
    defaultValue: string
) => {
    return (
        <label>
            {label}
            <br />
            <input
                type="text"
                defaultValue={defaultValue}
                readOnly={true}
            />
        </label>
    )
}

export const displayNumberField = (
    label: string,
    defaultValue: number,
    action: any,
    isInt: boolean
) => {
    return (
        <label>
            {label}
            <br />
            <input
                type="number"
                value={defaultValue}
                onChange={(e) => {
                    if (isInt) {
                        return store.dispatch(action(
                            parseInt(e.currentTarget.value)
                        ));
                    }
                    store.dispatch(action(
                        parseFloat(e.currentTarget.value)
                    ));
                }}
            />
        </label>
    )
}

export const displayPosField = (
    label: string[],
    defaultValue: pos,
    action: any[]
) => {
    return (
        <div className='pos'>
            <p>{label[0]}</p>
            <label>
                {label[1]}
                <br />
                <input
                    type="number"
                    value={defaultValue.x}
                    onChange={(e) => {
                        store.dispatch(action[0](
                            Math.round(parseFloat(e.currentTarget.value))
                        ));
                    }}
                />
            </label>
            <label>
                {label[2]}
                <br />
                <input
                    type="number"
                    value={defaultValue.y}
                    onChange={(e) => {
                        store.dispatch(action[1](
                            Math.round(parseFloat(e.currentTarget.value))
                        ));
                    }}
                />
            </label>
        </div>
    )
}

export const displayBoolField = (
    label: string,
    id: string,
    defaultValue: boolean,
    action: any
) => {
    return (
        <div className="checkbox">
            <p onChange={(e) => {
                let input = e.currentTarget
                    .parentElement
                    ?.querySelector('input');
                if (input) {
                    input.click();
                }
            }}>{label}</p>
            <div className="rel">
                <input
                    id={id}
                    className="checkbox"
                    type="checkbox"
                    checked={defaultValue}
                    onChange={(e) => {
                        store.dispatch(action(e.currentTarget.checked));
                    }}
                />
                <label htmlFor={id}>
                    <span>NO</span>
                </label>
            </div>
        </div>
    )
}

export const displaySeparator = (label: string) => {
    return (
        <div className="separator">
            <span></span>
            <p>{label}</p>
        </div>
    )
}
