import React from "react";
import { ipcRenderer, IpcRendererEvent } from "electron";
import fs from 'fs';
import jetpack from "fs-jetpack";

import '../App.global.css';
import MapView from './MapView';
import DetailsView from './DetailsView';
import Resizer from './Resizer';
import GalaxyParser from "../parser/GalaxyParser";
import Exporter from "../exporter/exporter";
import store from "../store";
import { reset as resetMap } from "../reducers/mapSettingsReducer";
import { reset as resetStar } from "../reducers/starReducer";
import { reset as resetPlanet } from "../reducers/planetReducer";
import { reset as resetPlanetItems } from "../reducers/planetItemsReducer";
import { reset as resetGroup } from "../reducers/groupReducer";
import { reset as resetCondition } from "../reducers/conditionReducer";
import { reset as resetConnection } from "../reducers/connectionReducer";
import { reset as resetISC } from "../reducers/interStarConnectionReducer";
import { reset as resetPlayer} from "../reducers/playerReducer";
import { reset as resetTemplate } from "../reducers/templateReducer";
import { setIsLoaded } from "../reducers/mapLoadedReducer";

const MainView = () => {

    const resetAll = () => {
        const dispatch = store.dispatch;
        dispatch(setIsLoaded(false));
        dispatch(resetMap());
        dispatch(resetStar());
        dispatch(resetPlanet());
        dispatch(resetPlanetItems());
        dispatch(resetGroup());
        dispatch(resetCondition());
        dispatch(resetConnection());
        dispatch(resetISC());
        dispatch(resetPlayer());
        dispatch(resetTemplate());
    }

    ipcRenderer.on("selectedFile", (event: IpcRendererEvent, selectedfolders: string[]) => {
        if (selectedfolders[0] && fs.existsSync(selectedfolders[0])) {
            resetAll();
            let data = fs.readFileSync(selectedfolders[0], 'utf8').split('\n')
            GalaxyParser({ galaxyLines: data });
        }
    });

    ipcRenderer.on("saveFile", (event: IpcRendererEvent, savePath: string) => {
        if (savePath){ // && store.getState().mapLoader.isLoaded) {
            let content = Exporter();
            jetpack.file(savePath, { content });
        }
    });

    return (
        <div>
            <MapView />
            <Resizer />
            <DetailsView />
        </div>
    );
};

export default MainView;
