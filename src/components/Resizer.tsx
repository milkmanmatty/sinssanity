import React from 'react';
import { useAppDispatch, useAppSelector } from '../hooks/hooks';
import useWindowSize from '../hooks/useWindowSize';
import { setToAmount } from '../reducers/detailsSizeReducer';
import { setMouseEvent } from '../reducers/uiReducer';

const Resizer = () => {
    const dispatch = useAppDispatch();
    const size = useAppSelector(state => state.detailsSize.value);
    const window = useWindowSize();
    const delta = 3;

    const onDragEnd = (event:React.MouseEvent) => {
        const distFromRight = window.x - event.pageX;
        const diffX = Math.abs(distFromRight - size);
        if (diffX >= delta) {
            dispatch(
                setToAmount(
                    Math.max(Math.min(distFromRight, 500), 150)
                )
            );
        }
        dispatch(setMouseEvent(''))
    }

    return (
        <div
            id="resizer"
            onMouseDown={ () => dispatch(setMouseEvent('RESIZER')) }
            onDragEnd={ (event:React.MouseEvent) => onDragEnd(event) }
            draggable="true"
        ></div>
    );
};

export default Resizer;
