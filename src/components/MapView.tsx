import React, { useRef, useState, MouseEvent } from 'react';
import { useAppDispatch, useAppSelector } from '../hooks/hooks';
import { updatePlanet, updateStar } from '../reducers/mapSettingsReducer';
import { planet, setPos as setPosPlanet } from '../reducers/planetReducer';
import { setPosUpdatePlanets } from '../reducers/starReducer';
import { setCursor, setMouseEvent, setViewportX, setViewportY } from '../reducers/uiReducer';
import store from '../store';
import Galaxy from './galaxy/Galaxy';

const MapView = () => {
    const state = useAppSelector(state => state);
    const ui = useAppSelector(state => state.ui);
    const dispatch = useAppDispatch();
    const [mouseDownLocX, setMouseDownLocX] = useState(0);
    const [mouseDownLocY, setMouseDownLocY] = useState(0);
    const mapRef = useRef<HTMLDivElement>(null);

    const renderEntities = () => {
        if (state.mapLoader.isLoaded) {
            return <Galaxy onZoom={onZoom} />
        }
        return "MapView"
    }

    const onMouseDown = (e: MouseEvent) => {
        if (ui.keyEvent === 'PANING') {
            dispatch(setCursor('grabbing'));
            dispatch(setMouseEvent('PANING'));
            setMouseDownLocX(-1 * e.clientX);
            setMouseDownLocY(-1 * e.clientY);
        }
        else if (ui.mouseEvent === 'DRAG_PLANET' ||
            (e.target as HTMLElement).classList.contains('planet-centre')
        ) {
            dispatch(setCursor('move'));
            setMouseDownLocX(e.clientX);
            setMouseDownLocY(e.clientY);
        }
    };

    const onDragEnd = (e: MouseEvent) => {
        if (ui.mouseEvent === 'DRAG_PLANET' ||
            ui.mouseEvent === 'DRAG_STAR'
        ) {
            handleDragMove(e);
            console.log("DragEndMV");
        }
        handleMouseUp(e);
    };
    const onMouseUp = (e: MouseEvent) => {
        handleMouseUp(e);
    };

    const handleMouseUp = (e: MouseEvent) => {
        if (ui.mouseEvent === 'PANING') {
            dispatch(setMouseEvent(''));
            if (ui.keyEvent === 'PANING') {
                dispatch(setCursor('grab'));
            } else {
                dispatch(setCursor('initial'));
            }
        }
        else if (ui.mouseEvent === 'DRAG_PLANET') {
            dispatch(setMouseEvent(''));
            dispatch(setCursor('initial'));
            if(
                mouseDownLocX - (e.clientX) >  1.0 ||
                mouseDownLocX - (e.clientX) < -1.0 ||
                mouseDownLocY - (e.clientY) >  1.0 ||
                mouseDownLocY - (e.clientY) < -1.0
            ){
                dispatch(updatePlanet(state.planetReducer))
            }
        }
        else if (ui.mouseEvent === 'DRAG_STAR') {
            dispatch(setMouseEvent(''));
            dispatch(setCursor('initial'));
            if(
                mouseDownLocX - (e.clientX) >  1.0 ||
                mouseDownLocX - (e.clientX) < -1.0 ||
                mouseDownLocY - (e.clientY) >  1.0 ||
                mouseDownLocY - (e.clientY) < -1.0
            ){
                dispatch(updateStar(state.starReducer));
                console.log('update',ui.mouseEvent);
            } else {
                console.log('stop',ui.mouseEvent);
            }
        }
    };

    const onMouseMove = (e: MouseEvent) => {
        if (ui.mouseEvent === 'PANING' &&
            ui.keyEvent === 'PANING' &&
            mapRef.current
        ) {
            mapRef.current.scrollLeft = (
                mapRef.current.scrollLeft +
                (-1 * e.clientX) -
                mouseDownLocX
            );
            mapRef.current.scrollTop = (
                mapRef.current.scrollTop +
                (-1 * e.clientY) -
                mouseDownLocY
            );
            dispatch(setViewportX(mapRef.current.scrollLeft));
            dispatch(setViewportY(mapRef.current.scrollTop));
            setMouseDownLocX((-1 * e.clientX));
            setMouseDownLocY((-1 * e.clientY));
        }
        else if (ui.mouseEvent === 'DRAG_PLANET' ||
                ui.mouseEvent === 'DRAG_STAR'
        ) {
            handleDragMove(e);
        }
    };

    const onZoom = (e: React.WheelEvent, scaleDelta: number) => {
        if (mapRef.current) {
            const percentX = e.clientX / mapRef.current.offsetWidth;
            const percentY = e.clientY / mapRef.current.offsetHeight;

            mapRef.current.scrollLeft += (
                percentX *
                scaleDelta *
                (state.mapSettings.galaxyWidth)
            );
            mapRef.current.scrollTop += (
                percentY *
                scaleDelta *
                (state.mapSettings.galaxyHeight)
            );
            dispatch(setViewportX(mapRef.current.scrollLeft));
            dispatch(setViewportY(mapRef.current.scrollTop));
        }
    }

    return (
        <div
            id="map"
            onMouseDown={(e: MouseEvent) => onMouseDown(e)}
            onMouseMove={(e: MouseEvent) => onMouseMove(e)}
            onMouseUp={(e: MouseEvent) => onMouseUp(e)}
            onDrag={(e: MouseEvent) => onMouseMove(e)}
            onDragEnd={(e: MouseEvent) => onDragEnd(e)}
            ref={mapRef}
        >
            {renderEntities()}
        </div>
    );
};

const handleDragMove = (e:MouseEvent) => {
    const ui = store.getState().ui;
    const dispatch = store.dispatch;
    const mouseX = (e.clientX + ui.viewportX) / ui.scale;
    const mouseY = (e.clientY + ui.viewportY) / ui.scale;

    if (ui.curSelection === "PLANET" &&
        ui.mouseEvent === 'DRAG_PLANET'
    ){
        if((ui.curSelectionEntity as planet).index ===
            store.getState().planetReducer.index
        ){
            console.log(store.getState().planetReducer);
            dispatch(setPosPlanet({
                x: Math.round(mouseX),
                y: Math.round(mouseY)
            }));
        } else {
            console.error(
                "wrong!",
                (ui.curSelectionEntity as planet),
                store.getState().planetReducer
            );
        }

    } else {
        dispatch(setPosUpdatePlanets({
            x: Math.round(mouseX),
            y: Math.round(mouseY)
        }));
    }
}

export default MapView;
