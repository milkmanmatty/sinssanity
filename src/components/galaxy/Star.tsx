import React from 'react';
import { WithChildren } from '../../App';
import { useAppDispatch, useAppSelector } from '../../hooks/hooks';
import { star } from '../../reducers/starReducer';
import { setMouseEvent } from '../../reducers/uiReducer';
import Connection from './Connection';
import { updateSelection } from './Galaxy';
import Planet from './Planet';

type StarProps = WithChildren<{
    star: star,
    scale: number
}>

const Star = (props: StarProps) => {
    const ui = useAppSelector(state => state.ui);
    const dispatch = useAppDispatch();

    const base = props.star.radius * 2;
    const normalisedRadius = {
        width: (base * props.scale) + 'px',
        height: (base * props.scale) + 'px'
    }

    const style = {
        height: normalisedRadius.height,
        width: normalisedRadius.width,
        top: (props.star.pos.y - props.star.radius) * props.scale,
        left: (props.star.pos.x - props.star.radius) * props.scale
    }

    const centreStyle = {
        width: Math.min(32.5, (Math.max(7.5 * props.scale, 1))),
        height: Math.min(32.5, (Math.max(7.5 * props.scale, 1)))
    }

    const dragStart = (e: React.MouseEvent) => {
        if (ui.keyEvent === 'PANING') {
            e.preventDefault();
            e.stopPropagation();
            return false;
        }
        console.log('start DRAG_STAR');
        dispatch(setMouseEvent('DRAG_STAR'));
        return true;
    }

    const renderPlanets = () => {
        return props.star.planetArray.map((planet,index) => {
            return <Planet
                planet={planet}
                planetIndex={index}
                starPos={props.star.pos}
                starRadius={props.star.radius}
                key={index}
            />
        });
    }

    const renderConnections = () => {
        return props.star.connectionArray.map((con,index) => {
            return <Connection
                con={con}
                star={props.star}
                key={index}
            />
        });
    }

    return (
        <div id="star" style={style}>
            <div
                className="star-centre"
                style={centreStyle}
                onClick={(e: React.MouseEvent) => updateSelection(e,props.star)}
                onDragStart={(e: React.MouseEvent) => dragStart(e)}
                onMouseDown={(e: React.MouseEvent) => dragStart(e)}
            ></div>
            <div className="radius" style={normalisedRadius}></div>
            {renderPlanets()}
            {renderConnections()}
        </div>
    );
};

export default Star;
