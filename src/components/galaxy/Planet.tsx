import React from 'react';
import { WithChildren } from '../../App';
import { useAppDispatch, useAppSelector } from '../../hooks/hooks';
import { setPlanetCount } from '../../reducers/mapSettingsReducer';
import { planet, setIndex } from '../../reducers/planetReducer';
import { pos } from '../../reducers/starReducer';
import { setMouseEvent } from '../../reducers/uiReducer';
import store from '../../store';
import { updateSelection } from './Galaxy';

type PlanetProps = WithChildren<{
    planet: planet,
    planetIndex: number,
    starPos: pos,
    starRadius: number
}>

const Planet = (props: PlanetProps) => {
    const ui = useAppSelector(state => state.ui);
    const dispatch = useAppDispatch();

    const relativeX = (
        (
            props.planet.pos.x -
            props.starPos.x +
            props.starRadius
        ) * ui.scale
    );
    const relativeY = (
        (
            props.planet.pos.y -
            props.starPos.y +
            props.starRadius
        ) * ui.scale
    );
    const style = {
        top: relativeY,
        left: relativeX,
        width: Math.min(30, (Math.max(5 * ui.scale, 1))),
        height: Math.min(30, (Math.max(5 * ui.scale, 1)))
    }

    const getIconForPlanet = () => {
        return "";
    }

    const handleMouseDown = (e: React.MouseEvent) => {
        dragStart(e);
    }

    const handleDragStart = (e: React.MouseEvent) => {
        console.log('drag-start',props.planet.index);
        updateSelection(e, props.planet);
        dragStart(e);
    }

    const dragStart = (e: React.MouseEvent) => {
        if (ui.keyEvent === 'PANING') {
            e.preventDefault();
            e.stopPropagation();
            return false;
        }
        dispatch(setMouseEvent('DRAG_PLANET'));
        return true;
    }

    return (
        <div
            className="planet"
            style={style}
            data-i={props.planetIndex}
            onDragStart={(e: React.MouseEvent) => handleDragStart(e)}
            onMouseDown={(e: React.MouseEvent) => handleMouseDown(e)}
            draggable={true}
        >
            <div className="planet-centre"
                onMouseDown={(e: React.MouseEvent) => updateSelection(e, props.planet, true)}
            />
            <div className="icon">
                {getIconForPlanet()}
            </div>
        </div>
    );
};

export default Planet;

export const recomputePlanetIndicies = () => {
    let index = 0;
    for (let s = 0; s < store.getState().mapSettings.starArray.length; s++) {
        for (let p = 0; p < store.getState().mapSettings.starArray[s].planetArray.length; p++) {
            store.dispatch(setIndex(index));
            index++;
        }
    }
    store.dispatch(setPlanetCount(index + 1));
}
