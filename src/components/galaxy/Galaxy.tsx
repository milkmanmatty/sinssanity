import React, { useEffect, useRef } from 'react';
import { useAppDispatch, useAppSelector } from '../../hooks/hooks';
import { connection, loadConnection } from '../../reducers/connectionReducer';
import { mapSettings, setPixelsPerGalaxyUnit } from '../../reducers/mapSettingsReducer';
import { loadPlanet, planet } from '../../reducers/planetReducer';
import { loadStar, star } from '../../reducers/starReducer';
import { setCursor, setKeyEvent, setScale, setSelection, setSelectionEntity } from '../../reducers/uiReducer';
import store from '../../store';
import { typeCheckEntity } from '../DetailsView';
import { recomputePlanetIndicies } from './Planet';
import Star from './Star';

interface GalaxyProps {
    onZoom: any
}

const MAX_SCALE = 10;
const MIN_SCALE = 1;

const Galaxy = (props: GalaxyProps) => {
    const dispatch = useAppDispatch();
    const state = useAppSelector(state => state.mapSettings);
    const ui = useAppSelector(state => state.ui);
    const galRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
        recomputePlanetIndicies();
    }, [state.starArray]);

    const keyDownCBRef = useRef<any>();
    const keyUpCBRef = useRef<any>();
    useEffect(() => {
        keyDownCBRef.current = stopSpaceScroll;
        keyUpCBRef.current = onKeyUp;
        document.body.addEventListener('keydown', (e: any) => keyDownCBRef.current(e));
        document.body.addEventListener('keyup', (e: any) => keyUpCBRef.current(e));
        return function cleanup() {
            window.removeEventListener('keydown', (e: any) => keyDownCBRef.current(e));
            window.removeEventListener('keyup', (e: any) => keyUpCBRef.current(e));
        }
    }, [keyDownCBRef, keyUpCBRef]);

    useEffect(() => {
        keyDownCBRef.current = stopSpaceScroll;
        keyUpCBRef.current = onKeyUp;
    }, [ui.keyEvent]);

    useEffect(() => {
        if (document && document.getElementById('galaxy')) {
            document.getElementById('map')!.style.display = 'none';
            document.getElementById('map')!.style.display = 'block';
            document.getElementById('galaxy')!.style.cursor = ui.cursor;
        }
    }, [ui.cursor]);

    const stopSpaceScroll = (e: React.KeyboardEvent) => {
        if (e.key === ' ' && e.target == document.body) {
            e.preventDefault();
            if (ui.keyEvent !== 'PANING') {
                store.dispatch(setKeyEvent('PANING'));
                store.dispatch(setCursor('grab'));
            }
        }
    }
    const onKeyUp = (e: React.KeyboardEvent) => {
        if (e.key === ' ' &&
            e.target == document.body &&
            ui.keyEvent === 'PANING'
        ) {
            dispatch(setKeyEvent(''));
            dispatch(setCursor('initial'));
            console.log('reset key');
        }
    }

    const handleOnWheel = (event: React.WheelEvent) => {
        const delta = event.deltaY < 0 ? 0.1 : -0.1;

        if ((ui.scale >= MAX_SCALE && delta > 0) ||
            (ui.scale <= MIN_SCALE && delta < 0)
        ) {
            return;
        }

        props.onZoom(event, delta);
        dispatch(setScale(
            Math.min(Math.max(ui.scale + delta, MIN_SCALE), MAX_SCALE)
        ));

        dispatch(setPixelsPerGalaxyUnit(
            Math.min(Math.max(state.pixelsPerGalaxyUnit + delta, 1), 10)
        ));
    }
    useEffect(() => {
        const el = (e: Event) => {
            if (isSubTypeWithProps<Event, WheelEvent>(e, 'deltaY')) {
                e.preventDefault();
            }
        }

        if (document.querySelector('#galaxy')) {
            document.querySelector('#galaxy')!
                .addEventListener(
                    "wheel",
                    (e: Event) => el(e),
                    { passive: false });
        }
        return () => document.body.removeEventListener('wheel', el)
    }, []);

    const styles = {
        width: state.galaxyWidth * ui.scale,
        height: state.galaxyHeight * ui.scale,
        cursor: 'initial'
    }

    const renderStars = () => {
        return state.starArray.map((star) => {
            return <Star star={star} scale={ui.scale} key={star.designName} />
        });
    }

    const handleClick = (e: React.MouseEvent) => {
        if (e.currentTarget === e.target ||
            (e.target as HTMLElement).classList.contains('star') ||
            (e.target as HTMLElement).classList.contains('radius')
        ) {
            updateSelection(e, state);
        }
    }

    return (
        <div
            id="galaxy"
            style={styles}
            onClick={(e: React.MouseEvent) => handleClick(e)}
            onWheel={(e: React.WheelEvent) => handleOnWheel(e)}
            onDragOver={(e: React.MouseEvent) => e.preventDefault()}
            ref={galRef}
            data-scale={ui.scale}
        >
            {renderStars()}
        </div>
    );
};

export const updateSelection = (
    event: React.MouseEvent,
    sel: mapSettings | star | planet | connection | undefined,
    propagate = false
) => {
    if (store.getState().ui.mouseEvent !== '') {
        return;
    }

    if (!propagate) {
        event.preventDefault();
        event.stopPropagation();
    }
    let actives = document.querySelectorAll('.active');
    for (let i = 0; i < actives.length; i++) {
        actives[i].classList.remove('active');
    }

    const dispatch = store.dispatch;
    const entityType = typeCheckEntity(sel);
    dispatch(setSelection(entityType));
    switch (entityType) {
        case "STAR":
            dispatch(loadStar(sel as star));
            event.currentTarget.classList.add('active');
            break;
        case "PLANET":
            dispatch(loadPlanet(sel as planet));
            event.currentTarget.parentElement!.classList.add('active');
            break;
        case "CONNECTION":
            dispatch(loadConnection(sel as connection));
            event.currentTarget.classList.add('active');
            break;
        case "MAP":
        case "UNKNOWN":
        default:
            break;
    }
    dispatch(setSelectionEntity(sel));
}

/**
 * Checks whether an object can be safely cast to its child type
 * @param parent the object to be 'narrowly' cast down to its child type
 * @param checkForProps props which aught to be present on the child type
 */
export function isSubTypeWithProps<P, C extends P>(parent: P, ...checkForProps: (keyof C)[]): parent is C {
    return checkForProps.every(prop => prop in parent);
}

export default Galaxy;
