import React from 'react';
import { useAppSelector } from '../../hooks/hooks';
import { connection } from '../../reducers/connectionReducer';
import { star } from '../../reducers/starReducer';
import { updateSelection } from './Galaxy';

type ConnectionProps = {
    con: connection,
    star: star,
}

const Connection = (props: ConnectionProps) => {
    const ui = useAppSelector(state => state.ui);
    const planetA = props.con.planetIndexA === -1 ?
        props.star :
        props.star.planetArray[props.con.planetIndexA];
    const planetB = props.con.planetIndexB === -1 ?
        props.star :
        props.star.planetArray[props.con.planetIndexB];

    const getDist = () => {
        const distX = Math.abs(
            planetA.pos.x -
            planetB.pos.x
        );
        const distY = Math.abs(
            planetA.pos.y -
            planetB.pos.y
        );
        return Math.sqrt(Math.pow(distX,2) + Math.pow(distY,2));
    }
    const relativeX = (
        (
            (Math.abs(
                planetA.pos.x +
                planetB.pos.x
            ) * 0.5) -
            props.star.pos.x +
            props.star.radius
        ) * ui.scale
    );
    const relativeY = (
        (
            (Math.abs(
                planetA.pos.y +
                planetB.pos.y
            ) * 0.5) -
            props.star.pos.y +
            props.star.radius
        ) * ui.scale
    );
    const rotation = () => {
        const oppo = (((planetA.pos.y + planetB.pos.y) * 0.5) -
            planetA.pos.y);

        const adj = (((planetA.pos.x + planetB.pos.x) * 0.5) -
        planetA.pos.x);

        return Math.atan(oppo/adj);
    };

    const style = {
        top: relativeY,
        left: relativeX,
        transform: "translate(-50%, -50%) rotate("+rotation()+"rad)",
        width: getDist() * ui.scale,
        height: 3
    }

    return (
        <div
            className="connection"
            style={style}
            data-a={props.con.planetIndexA}
            data-b={props.con.planetIndexB}
            onClick={(e: React.MouseEvent) => updateSelection(e,props.con)}
        ></div>
    );
};

export default Connection;
