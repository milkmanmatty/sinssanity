import React from 'react';
import { useAppSelector } from '../hooks/hooks';
import { connection } from '../reducers/connectionReducer';
import { mapSettings } from '../reducers/mapSettingsReducer'
import { planet } from '../reducers/planetReducer';
import { star } from '../reducers/starReducer';
import ConnectionEdit from './ui/ConnectionEdit';
import MapEdit from './ui/MapEdit';
import PlanetEdit from './ui/planetEdit';
import StarEdit from './ui/StarEdit';

const DetailsView = () => {
    const state = useAppSelector(state => state.ui);
    const isLoaded = useAppSelector(state => state.mapLoader.isLoaded);
    const size = useAppSelector(state =>
        state.detailsSize.value
    )

    const renderUI = () => {
        if(!isLoaded || !state.curSelection){
            return;
        }

        //check if curSelection is of mapSettings type
        if(typeCheckEntity(state.curSelectionEntity) === 'MAP'){
            return (
                <MapEdit />
            )
        }
        if(typeCheckEntity(state.curSelectionEntity) === 'STAR'){
            return (
                <StarEdit />
            )
        }
        if(typeCheckEntity(state.curSelectionEntity) === 'PLANET'){
            return (
                <PlanetEdit />
            )
        }
        if(typeCheckEntity(state.curSelectionEntity) === 'CONNECTION'){
            return (
                <ConnectionEdit />
            )
        }

        return <div>DetailsView</div>
    }

    return (
        <div id="details" style={{width: size, minWidth: size}}>
            <div className="details-padding">
                {renderUI()}
            </div>
        </div>
    );
};

export const typeCheckEntity = (
    entity: mapSettings | star | planet | connection | undefined,
) => {
    if(!entity){
        return 'UNKNOWN';
    }
    if((entity as mapSettings).starArray){
        return 'MAP';
    }
    if((entity as star).radius){
        return 'STAR';
    }
    if((entity as planet).owner !== undefined &&
        (entity as planet).owner !== null){
        return 'PLANET';
    }
    if((entity as connection).planetIndexA !== undefined &&
        (entity as connection).planetIndexA !== null){
        return 'CONNECTION';
    }
    return 'UNKNOWN';
}

export default DetailsView;
