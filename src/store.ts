import { configureStore } from '@reduxjs/toolkit'
import conditionReducer from './reducers/conditionReducer';
import connectionReducer from './reducers/connectionReducer';
import detailsSizeReducer from './reducers/detailsSizeReducer'
import groupReducer from './reducers/groupReducer';
import interStarConnectionReducer from './reducers/interStarConnectionReducer';
import mapLoadedReducer from './reducers/mapLoadedReducer';
import mapSettingsReducer from './reducers/mapSettingsReducer';
import planetItemsReducer from './reducers/planetItemsReducer';
import planetReducer from './reducers/planetReducer';
import playerReducer from './reducers/playerReducer';
import starReducer from './reducers/starReducer';
import templateReducer from './reducers/templateReducer';
import uiReducer from './reducers/uiReducer';

const store = configureStore({
    reducer: {
        detailsSize: detailsSizeReducer,
        ui: uiReducer,
        mapLoader: mapLoadedReducer,

        //Parsers
        mapSettings: mapSettingsReducer,
        starReducer: starReducer,
        planetReducer: planetReducer,
        planetItemsReducer: planetItemsReducer,
        groupReducer: groupReducer,
        conditionReducer: conditionReducer,
        connectionReducer: connectionReducer,
        interStarConnectionReducer: interStarConnectionReducer,
        playerReducer: playerReducer,
        templateReducer: templateReducer
    }
})

export default store;

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch
