import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "./hooks";
import { setWindowSize } from "../reducers/uiReducer";

const useWindowSize = () => {
    const dispatch = useAppDispatch();
    const size = useAppSelector(state => state.ui.windowSize);

    useEffect(() => {
        // Handler to call on window resize
        function handleResize() {
            // Set window width/height to state
            dispatch(setWindowSize({
                x: window.innerWidth,
                y: window.innerHeight,
            }))
        }

        // Add event listener
        window.addEventListener("resize", handleResize);

        // Call handler right away so state gets updated with initial window size
        handleResize();

        // Remove event listener on cleanup
        return () => window.removeEventListener("resize", handleResize);
    }, []); // Empty array ensures that effect is only run on mount

    return size;
}

export default useWindowSize;
