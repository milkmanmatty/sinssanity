import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
import MainView from './components/MainView';


export type WithChildren<T = {}> =
    T & { children?: React.ReactNode };

export default function App() {
    return (
        <Provider store={store}>
            <Router>
                <Switch>
                    <Route path="/" component={MainView} />
                </Switch>
            </Router>
        </Provider>
    );
}
